<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Guru
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role == 'guru') {
            return $next($request);
        }
        elseif (Auth::check() && Auth::user()->role == 'siswa') {
            return redirect('/');
        }
        else {
            return redirect('/haiAdmin');
        }
    }
}
