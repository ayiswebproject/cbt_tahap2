<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Siswa;
use App\Classes;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Yajra\Datatables\Datatables;
use App\Imports\SiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class SiswaController extends Controller
{

    public function index()
    {
        $class = Classes::all();
        return view('admin.siswa.siswaList', compact('class'));
    }
    
    public function getData()
    {
        $siswa = DB::table('students')->join('users', 'students.user_id', '=', 'users.id')->join('classes', 'students.class_id', '=', 'classes.id')
                    ->select('users.id', 'students.name', 'users.username', 'users.email', 'students.gender', 'classes.class')
                    ->get();
        return Datatables::of($siswa)
        ->addColumn('action', function($siswa) {
            return "<a href='javascript:void(0);' class='btn btn-sm btn-outline-info detail' data-toggle='modal' data-target='#detail_modal' id='".$siswa->id."' title='Detail'>
            <i class='fa fa-search'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-secondary edit-akses' data-toggle='modal' data-target='#akses_modal' id='".$siswa->id."' title='Edit Akses User'>
            <i class='fa fa-lock'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-warning edit-profil' data-toggle='modal' data-target='#profil_modal' id='".$siswa->id."' title='Edit Profil'>
            <i class='fa fa-edit'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-danger delete' id='".$siswa->id."' title='Hapus'>
            <i class='fa fa-trash'></i></a>";
        })
        ->make(true);
    }

    public function addForm()
    {
        return view('admin.siswa.addSiswa');
    }

    public function storeAkses(Request $request)
    {
        $this->validate($request, [
            'id' => ['required', 'string', 'max:191', 'unique:users'],
            'username' => ['required', 'string', 'max:191', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $data = new User();
        $data->id = $request->id;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->role = $request->role;
        $data->save();

        return redirect()->action('Admin\SiswaController@confirmSuccess');
    }

    public function confirmSuccess()
    {
        $siswa = User::where('role', '=', 'siswa')->orderBy('created_at', 'desc')->limit(1)->get();
        return view('admin.siswa.confirmationNextStep', compact('siswa'));
    }

    public function formProfile($id)
    {
        $class = Classes::all();
        $siswa = User::where('role', '=', 'siswa')->orderBy('created_at', 'desc')->limit(1)->get();
        return view('admin.siswa.addBioSiswa', ['class' => $class, 'siswa' => $siswa]);
    }

    public function storeProfile(Request $request)
    {
        $data = new Siswa();
        $data->name = $request->name;
        $data->img_profil = $request->img_profil;
        $data->gender = $request->gender;
        $data->class_id = $request->class_id;
        $data->user_id = $request->user_id;
        $data->save();

        return redirect()->action('Admin\SiswaController@index')->with('alert-success', 'User Siswa Telah Berhasil Terdaftar!');
    }

    public function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $siswa = DB::table('students')->join('users', 'students.user_id', '=', 'users.id')->join('classes', 'students.class_id', '=', 'classes.id')->where('user_id', $id)->select('students.*', 'users.*', 'classes.*')->first();
        $output = array (
            'id' => $siswa->id,
            'name' => $siswa->name,
            'email' => $siswa->email,
            'username' => $siswa->username,
            'gender' => $siswa->gender,
            'class' => $siswa->class,
        );
        echo json_encode($output);
    }
    
    public function fetchdataAkses(Request $request)
    {
        $id = $request->input('id');
        $siswa = User::find($id);
        $output = array (
            'id' => $siswa->id,
            'email' => $siswa->email,
            'username' => $siswa->username,
        );
        echo json_encode($output);
    }

    public function fetchdataProfil(Request $request)
    {
        $id = $request->input('id');
        $siswa = Siswa::where('user_id',$id)->first();
        $output = array (
            'id' => $siswa->id,
            'name' => $siswa->name,
            'img_profil' => $siswa->img_profil,
            'gender' => $siswa->gender,
            'class_id' => $siswa->class_id,
            'user_id' => $siswa->user_id,
        );
        echo json_encode($output);
    }

    public function updateAkses(Request $request)
    {
        if ($request->get('check') == 'checked') {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
                'password' => ['required', 'confirmed'],
            ]);
        } else {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
            ]);
        }

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $siswa => $messages) {
                $error_array = $messages;

            }
        } else {
            if($request->get('button_action') == 'update') {
                $data = User::find($request->get('id'));
                $data->email = $request->get('email');
                $data->username = $request->get('username');
                if ($request->get('check') == 'checked') {
                    $data->password = bcrypt($request->get('password'));
                }
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Akses User</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function updateProfil(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'id' => 'required',
            'nameSiswa' => 'required',
            'gender' => 'required',
            'class_id' => 'required',
            'user_id' => 'required',
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $siswa => $messages) {
                $error_array = $messages;

            }
        } else {
            if($request->get('button_act') == 'updated') {
                $data = Siswa::find($request->get('id'));
                $data->name = $request->get('nameSiswa');
                $data->img_profil = $request->get('img_profil');
                $data->gender = $request->get('gender');
                $data->class_id = $request->get('class_id');
                $data->user_id = $request->get('user_id');
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Profil Siswa</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function delete(Request $request)
    {
        $siswa = User::find($request->input('id'));
        if($siswa->delete()) {
            echo 'Data Terhapus';
        }
    }

    public function importExcel() 
    {
        Excel::import(new SiswaImport,request()->file('import_file'));
           
        return back()->with('alert-success', 'Berhasil Import Data Siswa');
    }

}
