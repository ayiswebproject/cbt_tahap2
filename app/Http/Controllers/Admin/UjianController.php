<?php

namespace App\Http\Controllers\Admin;

use App\Mapel;
use App\Ujian;
use App\Soal;
use App\Hasil;
use App\ScoreEssay;
use App\KodeSoal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Validator;

class UjianController extends Controller
{

    public function index()
    {
        $mapel = Mapel::all();
        $log = KodeSoal::all();

        return view('admin.ujian.ujianList', ['mapel' => $mapel], ['log' => $log]);
    }

    public function indexEssay()
    {
        $mapel = Mapel::all();
        $log = KodeSoal::all();

        return view('admin.ujian.essayList', ['mapel' => $mapel], ['log' => $log]);
    }

    public function getData()
    {
        $test = DB::table('tests')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->join('users', 'tests.user_id', '=', 'users.id')
                ->select('tests.*', 'subjects.subjects', 'users.username')
                ->where('type_test', '=', 'Pilihan Ganda')
                ->get();
        return Datatables::of($test)
        ->addColumn('action', function($test) {
            return "<a href='javascript:void(0);' class='btn btn-sm btn-outline-info detail' data-toggle='modal' data-target='#detail_modal' id='".$test->id."' title='Detail'>
            <i class='fa fa-search'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-warning edit' data-toggle='modal' data-target='#edit_modal' id='".$test->id."' title='Edit'>
            <i class='fa fa-edit'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-danger delete' id='".$test->id."' title='Detail'>
            <i class='fa fa-trash'></i></a>
            <a href='ujian/editSoal/".$test->id."' class='btn btn-sm btn-outline-secondary see-soal' title='Lihat Soal'>
            <i class='fa fa-eye'></i> Lihat Soal</a>
            <a href='ujian/lihatHasil/".$test->id."' class='btn btn-sm btn-outline-success see-hasil' title='Lihat Hasil'>
            <i class='fa fa-check-square'></i> Lihat Hasil</a>";
        })
        ->make(true);
    }

    public function getEssay()
    {
        $test = DB::table('tests')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->join('users', 'tests.user_id', '=', 'users.id')
                ->select('tests.*', 'subjects.subjects', 'users.username')
                ->where('type_test', '=', 'Essay/Uraian')
                ->get();
        return Datatables::of($test)
        ->addColumn('action', function($test) {
            return "<a href='javascript:void(0);' class='btn btn-sm btn-outline-info detail' data-toggle='modal' data-target='#detail_modal' id='".$test->id."' title='Detail'>
            <i class='fa fa-search'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-warning edit' data-toggle='modal' data-target='#edit_modal' id='".$test->id."' title='Edit'>
            <i class='fa fa-edit'></i></a>
            <a href='javascript:void(0);' class='btn btn-sm btn-outline-danger delete' id='".$test->id."' title='Hapus'>
            <i class='fa fa-trash'></i></a>
            <a href='ujian/editSoal/".$test->id."' class='btn btn-sm btn-outline-secondary see-soal' title='Lihat Soal'>
            <i class='fa fa-eye'></i> Lihat Soal</a>
            <a href='ujian/getKoreksiEssay/".$test->id."' class='btn btn-sm btn-outline-dark see-soal' title='Koreksi Essay'>
            <i class='fa fa-book'></i> Koreksi Essay</a>
            <a href='ujian/lihatHasil/".$test->id."' class='btn btn-sm btn-outline-success see-hasil' title='Lihat Hasil'>
            <i class='fa fa-check-square'></i> Lihat Hasil</a>";
        })
        ->make(true);
    }

    public function getKoreksiEssay($id)
    {
        $essay = DB::table('results')
                    ->join('tests', 'results.test_id', 'tests.id')
                    ->join('users', 'results.user_id', 'users.id')
                    ->join('students', 'users.id', 'students.user_id')
                    ->join('subjects', 'tests.subjects_id', 'subjects.id')
                    ->select('results.test_id', 'results.id', 'results.user_id', 'results.results', 'users.username', 'tests.code_test', 'tests.name_test', 'tests.type_test', 'subjects.subjects', 'students.name')
                    ->where('results.test_id', $id)
                    ->get();

        return view('admin.ujian.listKoreksiEssay', ['essay' => $essay]);
    }

    public function addForm()
    {
        $mapel = Mapel::all();
        return view('admin.ujian.addUjian', compact('mapel'));
    }

    public function store(Request $request)
    {
        $data = new Ujian();
        $data->code_test = $request->code_test;
        $data->name_test = $request->name_test;
        $data->duration = $request->duration;
        $data->type_test = $request->type_test;
        $data->subjects_id = $request->subjects_id;
        $data->user_id = $request->user_id;
        $data->save();
        
        return redirect()->action('Admin\UjianController@addSoal', ['id' => $data->id]);
    }

    public function addSoal($id)
    {
        $check = DB::table('tests')
                    ->join('users', 'tests.user_id', '=', 'users.id')
                    ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                    ->select('tests.*', 'subjects.subjects', 'users.username')
                    ->where('tests.id', $id)
                    ->get();

        if ($check == null) {
            return view('admin.ujian.ujianList')->with(['alert-danger', 'Gagal, Kode Ujian Tidak Ditemukan!']);
        } else {
            return view('admin.ujian.addSoal', ['check' => $check]);
        }
    }

    public function editDetailSoal($id)
    {
        $detail = DB::table('tests')
                    ->join('users', 'tests.user_id', '=', 'users.id')
                    ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                    ->select('tests.*', 'subjects.subjects', 'users.username')
                    ->where('tests.id', $id)
                    ->get();

        $check = DB::table('questions')
                    ->join('tests', 'questions.test_id', '=', 'tests.id')
                    ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                    ->select('questions.*', 'tests.code_test', 'tests.name_test', 'tests.duration', 'tests.type_test', 'tests.jml_soal', 'subjects.subjects')
                    ->where('questions.id', $id)
                    ->get();

        if ($check == null) {
            return view('admin.ujian.ujianList')->with(['alert-danger', 'Gagal, Kode Ujian Tidak Ditemukan!']);
        } else {
            return view('admin.ujian.editDetailSoal', ['check' => $check]);
        }
    }

    public function storeSoal(Request $request)
    {
        $validation = Validator::make($request->all(), [

        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $soal => $messages) {
                $error_array = $messages;
            }
        } else {
            if ($request->get('button_action') == 'insert') {
                $test_id = $request->get('test_id');
                $questions = $request->get('questions');
                $file = $request->file('quest_img');
                if($file != null) {
                    $ext = $file->getClientOriginalName();
                    $quest_img = $ext;
                    $file->move('uploads/soal',$quest_img);
                } else if($file == null) {
                    $quest_img = null;
                }
                $a = $request->get('a');
                $b = $request->get('b');
                $c = $request->get('c');
                $d = $request->get('d');
                $e = $request->get('e');
                $answer_key = $request->get('answer_key');
                $answer_essay = $request->get('answer_essay');
        
                $data = new Soal();
                $data->test_id = $test_id;
                $data->questions = $questions;
                $data->quest_img = $quest_img;
                $data->a = $a;
                $data->b = $b;
                $data->c = $c;
                $data->d = $d;
                $data->e = $e;
                $data->answer_key = $answer_key;
                $data->answer_essay = $answer_essay;
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Tambah Soal Ujian</div>';
            }

            if($request->get('button_action') == 'update') {
                $data = Soal::find($request->get('id'));
                $data->test_id = $request->get('test_id');
                $data->questions = $request->get('questions');
                if($request->get('quest_img') != null) {
                    if (empty($request->file('quest_img'))) {
                        $data->quest_img = $data->quest_img;
                    }
                    else{
                        unlink('uploads/soal/'.$data->quest_img); //menghapus file lama
                        $file = $request->file('quest_img');
                        $ext = $file->getClientOriginalName();
                        $quest_img = $ext;
                        $file->move('uploads/soal',$quest_img);
                        $data->quest_img = $quest_img;
                    }
                } else {
                    $file = $request->file('quest_img');
                    if($file != null) {
                        $ext = $file->getClientOriginalName();
                        $quest_img = $ext;
                        $file->move('uploads/soal',$quest_img);
                        $data->quest_img = $quest_img;
                    } else if($file == null) {
                        $quest_img = null;
                    }
                }
                $data->a = $request->get('a');
                $data->b = $request->get('b');
                $data->c = $request->get('c');
                $data->d = $request->get('d');
                $data->e = $request->get('e');
                $data->answer_key = $request->get('answer_key');
                $data->answer_essay = $request->get('answer_essay');;
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Soal Ujian</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output,
            'hasil' => $data
        );
        echo json_encode($output);
    }

    public function updateDetail(Request $request, $id)
    {
        $data = Ujian::where('id',$id)->first();
        $data->code_test = $request->code_test;
        $data->name_test = $request->name_test;
        $data->duration = $request->duration;
        $data->jml_soal = $request->jml_soal;
        $data->save();

        return redirect()->route('ujian')->with('alert-success','Data dan Soal Ujian berhasil ditambahkan!');
    }

    public function updateDetailOnAdd(Request $request, $id)
    {
        $data = Ujian::where('id',$id)->first();
        $data->code_test = $request->code_test;
        $data->name_test = $request->name_test;
        $data->duration = $request->duration;
        $data->jml_soal = $request->jml_soal;
        $data->save();
        
        return redirect()->route('ujian.editSoal', $id)->with('alert-success','Soal Ujian berhasil ditambahkan!');
    }

    public function fetchdataUjian(Request $request)
    {
        $id = $request->input('id');
        $test = DB::table('tests')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->join('users', 'tests.user_id', '=', 'users.id')
                ->where('tests.id', $id)->select('tests.*', 'users.username', 'subjects.subjects')
                ->first();
        $output = array (
            'code_test' => $test->code_test,
            'name_test' => $test->name_test,
            'duration' => $test->duration,
            'type_test' => $test->type_test,
            'subjects' => $test->subjects,
            'username' => $test->username,
        );
        echo json_encode($output);
    }

    public function fetchdataEditUjian(Request $request)
    {
        $id = $request->input('id');
        $test = Ujian::where('id',$id)->first();
        $by = DB::table('tests')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->join('users', 'tests.user_id', '=', 'users.id')
                ->where('tests.id', $id)
                ->select('tests.*', 'users.username', 'subjects.subjects')
                ->first();
        $output = array (
            'id' => $test->id,
            'code_test' => $test->code_test,
            'name_test' => $test->name_test,
            'duration' => $test->duration,
            'type_test' => $test->type_test,
            'subjects_id' => $test->subjects_id,
            'username' => $by->username,
        );
        echo json_encode($output);
    }

    public function updateUjian(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name_test' => 'required',
            'duration' => 'required',
            'subjects_id' => 'required',
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $guru => $messages) {
                $error_array = $messages;

            }
        } else {
            if($request->get('button_action') == 'update') {
                $data = Ujian::find($request->get('id'));
                $data->code_test = $request->get('code_test');
                $data->name_test = $request->get('name_test');
                $data->duration = $request->get('duration');
                $data->type_test = $request->get('type_test');
                $data->subjects_id = $request->get('subjects_id');
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Data Ujian</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function editSoal($id)
    {
        $check = DB::table('tests')
                    ->join('users', 'tests.user_id', '=', 'users.id')
                    ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                    ->select('tests.*', 'subjects.subjects', 'users.username')
                    ->where('tests.id', $id)
                    ->get();

        $getSoal = DB::table('questions')
                    ->join('tests', 'questions.test_id', 'tests.id')
                    ->select('questions.*')
                    ->where('tests.id', $id)
                    ->get();

        return view('admin.ujian.editSoal', ['check' => $check], ['getSoal' => $getSoal]);
    }

    public function koreksiEssay($id, $ids)
    {
        $check = DB::table('results')
                    ->join('tests', 'results.test_id', 'tests.id')
                    ->join('users', 'results.user_id', 'users.id')
                    ->join('students', 'users.id', 'students.user_id')
                    ->join('questions', 'results.test_id', 'questions.test_id')
                    ->join('answers', 'questions.id', 'answers.quest_id')
                    ->select('answers.id', 'results.results', 'users.username', 'results.test_id', 'results.user_id', 'questions.questions', 'questions.quest_img', 'questions.answer_essay', 'answers.choice_essay', 'students.name')
                    ->where([
                        ['results.id', $id],
                        ['answers.user_id', $ids],
                    ])
                    ->get();

        $test_id = DB::table('results')
                    ->join('tests', 'results.test_id', 'tests.id')
                    ->join('users', 'results.user_id', 'users.id')
                    ->join('students', 'users.id', 'students.user_id')
                    ->join('questions', 'results.test_id', 'questions.test_id')
                    ->join('answers', 'questions.id', 'answers.quest_id')
                    ->select('tests.id', 'results.results', 'users.username', 'results.test_id', 'results.user_id', 'questions.questions', 'questions.quest_img', 'questions.answer_essay', 'answers.choice_essay', 'students.name')
                    ->where([
                        ['results.id', $id],
                        ['answers.user_id', $ids],
                    ])
                    ->get();

        return view('admin.ujian.koreksiEssay', ['check' => $check], ['test_id' => $test_id]);
    }

    public function addNilai(Request $request)
    {
        $id = $request->get('test_id');
        $user = $request->get('user_id');

        foreach ($request->score as $key => $value)
        {
            $answer_id = $request->answer_id[$key];
            $score = $value;

            $hasil = new ScoreEssay;
            $hasil->answer_id = $answer_id;
            $hasil->score = $score; 
            $hasil->save();
        }

        $score_total = DB::table('score_log')
                        ->join('answers', 'score_log.answer_id', 'answers.id')
                        ->join('questions', 'answers.quest_id', 'questions.id')
                        ->join('tests', 'questions.test_id', 'tests.id')
                        ->join('results', 'tests.id', 'results.test_id')
                        ->select('tests.id')
                        ->where([
                            ['results.test_id', $id],
                            ['results.user_id', $user]
                        ])
                        ->sum('score');


        $data = Hasil::where([
                    ['test_id', $id],
                    ['user_id', $user]
                ])
                ->first();
        $data->results = $score_total;
        $data->test_id = $request->test_id;
        $data->user_id = $request->user_id;
        $data->save();

        ScoreEssay::truncate();
        
        return redirect()->back()->with('alert-success','Berhasil Menambahkan Nilai');
    }

    public function lihatHasil($id)
    {
        $hasilUjian = DB::table('results')
                        ->join('tests', 'results.test_id', 'tests.id')
                        ->join('users', 'results.user_id', 'users.id')
                        ->join('students', 'users.id', 'students.user_id')
                        ->join('subjects', 'tests.subjects_id', 'subjects.id')
                        ->select('results.*', 'users.username', 'tests.*', 'subjects.subjects', 'students.name')
                        ->where('results.test_id', $id)
                        ->get();

        return view('admin.ujian.lihatHasil', ['hasilUjian' => $hasilUjian]);
    }

    public function delete(Request $request)
    {
        $test = Ujian::find($request->input('id'));
        if($test->delete()) {
            echo 'Data Terhapus';
        }
    }

    public function deleteLog()
    {
        KodeSoal::truncate();

        return redirect()->back()->with('alert-danger','Berhasil Menghapus Log Ujian');
    }
}
