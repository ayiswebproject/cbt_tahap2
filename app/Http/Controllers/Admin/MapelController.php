<?php

namespace App\Http\Controllers\Admin;

use App\Mapel;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class MapelController extends Controller
{

    public function index()
    {
        return view('admin.mapel.mapelList');
    }

    public function getData() {
        $mapel = Mapel::select('id', 'subjects');
        return Datatables::of($mapel)
        ->addColumn('action', function($mapel) {
            return '<a href="javascript:void(0);" class="btn btn-sm btn-outline-info detail" data-toggle="modal" data-target="#mapel_modal" id="'.$mapel->id.'" title="Detail">
            <i class="fa fa-search"></i></a>
            <a href="javascript:void(0);" class="btn btn-sm btn-outline-warning edit" data-toggle="modal" data-target="#mapel_modal" id="'.$mapel->id.'" title="Edit">
            <i class="fa fa-edit"></i></a>
            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger delete" id="'.$mapel->id.'" title="Hapus">
            <i class="fa fa-trash"></i></a>';
        })
        ->make(true);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'subjects_id' => 'required',
            'subjects' => 'required',
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $mapel => $messages) {
                $error_array = $messages;

            }
        } else {
            if ($request->get('button_action') == 'insert') {
                $data = new Mapel();
                $data->id = $request->subjects_id;
                $data->subjects = $request->subjects;
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Tambah Data Mapel</div>';
            }

            if($request->get('button_action') == 'update') {
                $data = Mapel::find($request->get('subjects_id'));
                $data->subjects = $request->get('subjects');
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Data Mapel</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $mapel = Mapel::find($id);
        $output = array (
            'id' => $mapel->id,
            'subjects' => $mapel->subjects
        );
        echo json_encode($output);
    }

    public function fetchedit(Request $request)
    {
        $id = $request->input('id');
        $mapel = Mapel::find($id);
        $output = array (
            'id' => $mapel->id,
            'subjects' => $mapel->subjects
        );
        echo json_encode($output);
    }

    public function delete(Request $request)
    {
        $mapel = Mapel::find($request->input('id'));
        if($mapel->delete()) {
            echo 'Data Terhapus';
        }
    }
}
