<?php

namespace App\Http\Controllers\Guru;

use Auth;
use App\User;
use App\Guru;
use App\Mapel;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class HomeAdminController extends Controller
{

    public function home()
    {
        $id = Auth::id();
        $guru = DB::table('teachers')->join('subjects', 'teachers.subjects_id', '=', 'subjects.id')->join('users', 'teachers.user_id', '=', 'users.id')->where('users.id', $id)->get();
        return view('guru.home', ['guru' => $guru]);
    }

    public function fetchdataAkses(Request $request)
    {
        $id = $request->input('id');
        $admin = User::find($id);
        $output = array (
            'email' => $admin->email,
            'username' => $admin->username,
        );
        echo json_encode($output);
    }

    public function updateAkses(Request $request)
    {
        if ($request->get('check') == 'checked') {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
                'password' => ['required', 'confirmed'],
            ]);
        } else {
            $validation = Validator::make($request->all(), [
                'email' => 'required',
                'username' => 'required',
            ]);
        }
        

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $admin => $messages) {
                $error_array = $messages;

            }
            $output = array(
                'error' => $error_array,
                'success' => $success_output
            );
        } else {
            if($request->get('button_action') == 'update') {
                $akses = User::find($request->get('id'));
                $akses->email = $request->get('email');
                $akses->username = $request->get('username');
                if ($request->get('check') == 'checked') {
                    $akses->password = bcrypt($request->get('password'));
                }
                $akses->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Akses User</div>';
            }
            $output = array(
                'email' => $akses->email,
                'username' => $akses->username,
                'error' => $error_array,
                'success' => $success_output
            );
        }
            
        echo json_encode($output);
    }
    
}
