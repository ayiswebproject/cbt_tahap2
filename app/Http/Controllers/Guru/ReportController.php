<?php

namespace App\Http\Controllers\Guru;

use Validator;
use App\Classes;
use App\Ujian;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use PDF;

class ReportController extends Controller
{

    public function index()
    {
        $class = Classes::all();
        $ujian = Ujian::all();
        return view('report.report', ['class' => $class], ['ujian' => $ujian]);
    }

    public function reportPDF(Request $request)
    {
        $id = $request->select_class;
        $ids = $request->select_code_test;

        $report = DB::table('classes')
                    ->join('students', 'classes.id', 'students.class_id')
                    ->join('users', 'students.user_id', 'users.id')
                    ->join('results', 'users.id', 'results.user_id')
                    ->join('tests', 'results.test_id', 'tests.id')
                    ->select('classes.class', 'students.name', 'users.id', 'results.results', 'tests.code_test', 'tests.name_test')
                    ->where([
                        ['classes.id', $id],
                        ['tests.id', $ids],
                    ])
                    ->get();

        $pdf = PDF::loadview('report.report_pdf', ['report' => $report]);
        $pdf->setPaper('A4', 'potrait');
        // return $pdf->stream();
        return $pdf->download('laporan_hasil.pdf');
    }
    
}
