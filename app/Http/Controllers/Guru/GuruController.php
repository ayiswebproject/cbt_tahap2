<?php

namespace App\Http\Controllers\Guru;

use App\User;
use App\Guru;
use App\Mapel;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Yajra\Datatables\Datatables;

class GuruController extends Controller
{

    public function index()
    {
        $mapel = Mapel::all();
        return view('guru.guru.guruList', compact('mapel'));
    }
    
    public function getData()
    {
        $guru = DB::table('teachers')->join('subjects', 'teachers.subjects_id', '=', 'subjects.id')->join('users', 'teachers.user_id', '=', 'users.id')->get();
        return Datatables::of($guru)
        ->addColumn('action', function($guru) {
            return "<a href='#' class='btn btn-sm btn-outline-info detail' data-toggle='modal' data-target='#detail_modal' id='".$guru->id."' title='Detail'>
            <i class='fa fa-search'></i></a>";
        })
        ->make(true);
    }

    public function addForm()
    {
        return view('guru.guru.addGuru');
    }

    public function storeAkses(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:191'],
            'username' => ['required', 'string', 'max:191', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $data = new User();
        $data->name = $request->name;
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->role = $request->role;
        $data->save();

        return redirect()->action('Guru\GuruController@confirmSuccess');
    }

    public function confirmSuccess()
    {
        $guru = User::where('role', '=', 'guru')->orderBy('created_at', 'desc')->limit(1)->get();
        return view('guru.guru.confirmationNextStep', compact('guru'));
    }

    public function formProfile($id)
    {
        $mapel = Mapel::all();
        $guru = User::where('role', '=', 'guru')->orderBy('created_at', 'desc')->limit(1)->get();
        return view('guru.guru.addBioGuru', ['mapel' => $mapel, 'guru' => $guru]);
    }

    public function storeProfile(Request $request)
    {
        $data = new Guru();
        $data->gender = $request->gender;
        $data->no_induk = $request->no_induk;
        $data->subjects_id = $request->subjects_id;
        $data->user_id = $request->user_id;
        $data->save();

        return redirect()->action('Guru\GuruController@index')->with('alert-success', 'User Guru Telah Berhasil Terdaftar!');
    }

    public function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $guru = DB::table('teachers')->join('users', 'teachers.user_id', '=', 'users.id')->join('subjects', 'teachers.subjects_id', '=', 'subjects.id')->where('user_id', $id)->select('teachers.*', 'users.*', 'subjects.*')->first();
        $output = array (
            'name' => $guru->name,
            'email' => $guru->email,
            'username' => $guru->username,
            'gender' => $guru->gender,
            'no_induk' => $guru->no_induk,
            'subjects' => $guru->subjects,
        );
        echo json_encode($output);
    }
    
    public function fetchdataAkses(Request $request)
    {
        $id = $request->input('id');
        $guru = User::find($id);
        $output = array (
            'name' => $guru->name,
            'email' => $guru->email,
            'username' => $guru->username,
        );
        echo json_encode($output);
    }

    public function fetchdataProfil(Request $request)
    {
        $id = $request->input('id');
        $guru = Guru::where('user_id',$id)->first();
        $output = array (
            'id' => $guru->id,
            'gender' => $guru->gender,
            'no_induk' => $guru->no_induk,
            'subjects_id' => $guru->subjects_id,
            'user_id' => $guru->user_id,
        );
        echo json_encode($output);
    }

    public function updateAkses(Request $request)
    {
        if ($request->get('check') == 'checked') {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
                'password' => ['required', 'confirmed'],
            ]);
        } else {
            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'username' => 'required',
            ]);
        }

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $guru => $messages) {
                $error_array = $messages;

            }
        } else {
            if($request->get('button_action') == 'update') {
                $data = User::find($request->get('id'));
                $data->name = $request->get('name');
                $data->email = $request->get('email');
                $data->username = $request->get('username');
                if ($request->get('check') == 'checked') {
                    $data->password = bcrypt($request->get('password'));
                }
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Akses User</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function updateProfil(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'id' => 'required',
            'gender' => 'required',
            'no_induk' => 'required',
            'subjects_id' => 'required',
            'user_id' => 'required',
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $guru => $messages) {
                $error_array = $messages;

            }
        } else {
            if($request->get('button_act') == 'updated') {
                $data = Guru::find($request->get('id'));
                $data->gender = $request->get('gender');
                $data->no_induk = $request->get('no_induk');
                $data->subjects_id = $request->get('subjects_id');
                $data->user_id = $request->get('user_id');
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Profil Guru</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function delete(Request $request)
    {
        $guru = User::find($request->input('id'));
        if($guru->delete()) {
            echo 'Data Terhapus';
        }
    }

}
