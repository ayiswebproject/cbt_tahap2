<?php

namespace App\Http\Controllers\Guru;

use App\Classes;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ClassController extends Controller
{

    public function index()
    {
        return view('guru.classroom.classList');
    }

    public function getData() {
        $class = Classes::select('id', 'class');
        return Datatables::of($class)
        ->addColumn('action', function($class) {
            return '<a href="javascript:void(0);" class="btn btn-sm btn-outline-info detail" data-toggle="modal" data-target="#class_modal" id="'.$class->id.'" title="Detail">
            <i class="fa fa-search"></i></a>
            <a href="javascript:void(0);" class="btn btn-sm btn-outline-warning edit" data-toggle="modal" data-target="#class_modal" id="'.$class->id.'" title="Edit">
            <i class="fa fa-edit"></i></a>
            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger delete" id="'.$class->id.'" title="Hapus">
            <i class="fa fa-trash"></i></a>';
        })
        ->make(true);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'class_id' => 'required',
            'class' => 'required',
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $class => $messages) {
                $error_array = $messages;

            }
        } else {
            if ($request->get('button_action') == 'insert') {
                $data = new Classes();
                $data->id = $request->class_id;
                $data->class = $request->class;
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Tambah Data Kelas</div>';
            }

            if($request->get('button_action') == 'update') {
                $data = Classes::find($request->get('class_id'));
                $data->class = $request->get('class');
                $data->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Edit Data Kelas</div>';
            }
        }
        $output = array(
            'error' => $error_array,
            'success' => $success_output
        );
        echo json_encode($output);
    }

    public function fetchdata(Request $request)
    {
        $id = $request->input('id');
        $class = Classes::find($id);
        $output = array (
            'id' => $class->id,
            'class' => $class->class
        );
        echo json_encode($output);
    }

    public function fetchedit(Request $request)
    {
        $id = $request->input('id');
        $class = Classes::find($id);
        $output = array (
            'id' => $class->id,
            'class' => $class->class
        );
        echo json_encode($output);
    }

    public function delete(Request $request)
    {
        $class = Classes::find($request->input('id'));
        if($class->delete()) {
            echo 'Data Terhapus';
        }
    }
}
