<?php

namespace App\Http\Controllers\Siswa;

use Auth;
use App\User;
use App\KodeSoal;
use App\Ujian;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class HomeSiswaController extends Controller
{

    public function home()
    {
        $id = Auth::id();
        $siswa = DB::table('students')->join('classes', 'students.class_id', '=', 'classes.id')->join('users', 'students.user_id', '=', 'users.id')->where('users.id', $id)->get();
        return view('siswa.home', ['siswa' => $siswa]);
    }

    public function storeKodeSoal(Request $request)
    {
        $ceknull = DB::table('test_log')
                ->where('user_log', $request->user_log)
                ->orderBy('created_at', 'desc')
                ->first();

        $tescode = DB::table('tests')
                ->where('code_test', $request->code_test_log)
                ->first();

        if (empty($tescode->code_test)) {

            return view('siswa.ujian.notFoundUjian');

        } else if ($request->code_test_log == $tescode->code_test) {
            if (empty($ceknull->code_test_log) && empty($ceknull->user_log)) {

                $data = new KodeSoal();
                $data->user_log = $request->user_log;
                $data->code_test_log = $request->code_test_log;
                $data->save();

                return redirect()->action('Siswa\ExamController@detailUjian', ['id' => $data->code_test_log]);

            } else if($ceknull->code_test_log != $tescode->code_test && $ceknull->user_log == auth()->user()->username) {

                $data = new KodeSoal();
                $data->user_log = $request->user_log;
                $data->code_test_log = $request->code_test_log;
                $data->save();

                return redirect()->action('Siswa\ExamController@detailUjian', ['id' => $data->code_test_log]);

            } else if ($ceknull->code_test_log == $tescode->code_test && $ceknull->user_log != auth()->user()->username) {

                $data = new KodeSoal();
                $data->user_log = $request->user_log;
                $data->code_test_log = $request->code_test_log;
                $data->save();

                return redirect()->action('Siswa\ExamController@detailUjian', ['id' => $data->code_test_log]);

            } else if($ceknull->code_test_log == $tescode->code_test && $ceknull->user_log == auth()->user()->username) {

                return view('siswa.ujian.sudahUjian');

            }
        }

    }

    public function fetchdataAkses(Request $request)
    {
        $id = $request->input('id');
        $siswa = User::find($id);
        $output = array (
            'id' => $siswa->id,
            'email' => $siswa->email,
            'username' => $siswa->username,
        );
        echo json_encode($output);
    }

    public function updateAkses(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'password' => ['required', 'confirmed'],
        ]);

        $error_array = array();
        $success_output = '';

        if ($validation->fails()) {
            foreach($validation->messages()->getMessages() as $siswa => $messages) {
                $error_array = $messages;

            }
         	$output = array(
	            'error' => $error_array,
	            'success' => $success_output
	        );
        } else {
            if($request->get('button_action') == 'update') {
                $akses = User::find($request->get('id'));
                $akses->email = $request->get('email');
                $akses->username = $request->get('username');
                $akses->password = bcrypt($request->get('password'));
                $akses->save();
                $success_output = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Berhasil Ganti Password</div>';
            }
            $output = array(
	            'error' => $error_array,
	            'success' => $success_output
	        );
        }
        
        echo json_encode($output);
    }
    
}
