<?php

namespace App\Http\Controllers\Siswa;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Mapel;
use App\Ujian;
use App\Soal;
use App\Jawaban;
use App\Hasil;
use Auth;

class ExamController extends Controller
{
	public function detailUjian(Request $request, $id)
	{
		$detail = DB::table('tests')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->where('code_test', $id)
                ->select('tests.*', 'subjects.subjects')
                ->get();

		return view('siswa.ujian.detailUjian', ['detail' => $detail]);
	}

	public function ujian($id)
	{
		$soal = DB::table('questions')
                ->join('tests', 'questions.test_id', '=', 'tests.id')
                ->join('subjects', 'tests.subjects_id', '=', 'subjects.id')
                ->select('questions.*', 'tests.code_test', 'tests.name_test', 'tests.duration', 'tests.type_test', 'tests.jml_soal', 'subjects.subjects')
                ->where('tests.code_test', $id)
                ->get();

        $timer = DB::table('tests')
        		->where('tests.code_test', $id)
        		->get();

        return view('siswa.ujian.startUjian', ['soal' => $soal], ['timer' => $timer]);
	}

	public function storeJawaban(Request $request)
	{
    	$jmlSoal = $request->jml_soal;
    	$benar = 0;
    	foreach ($request->question as $key => $question_id) {
			$question = Soal::find($question_id);
			$test = $question->test_id;

			if ($question->answer_key == $request->choice[$key]) {
				$benar++;
			}
			$answer = new Jawaban;
			$answer->choice = $request->choice[$key];
			$answer->quest_id = $question_id;
			$answer->user_id = Auth::id();
			$answer->save();

    	}

    	// Hitung Nilai
    	$nilai = $benar/$jmlSoal*100;

    	// Record Nilai ke DB
    	$result = new Hasil;
    	$result->results = $nilai;
    	$result->test_id = $test;
    	$result->user_id = Auth::id();
    	$result->save();

    	return view('siswa.ujian.selesaiUjian');
	}

	public function storeJawabanEssay(Request $request)
	{
    	foreach ($request->question as $key => $question_id) {
    		$question = Soal::find($question_id);
			$test = $question->test_id;

			$answer = new Jawaban;
			$answer->choice_essay = $request->choice_essay[$key];
			$answer->quest_id = $question_id;
			$answer->user_id = Auth::id();
			$answer->save();

    	}

    	// Record Nilai ke DB
    	$result = new Hasil;
    	$result->results = null;
    	$result->test_id = $test;
    	$result->user_id = Auth::id();
    	$result->save();

    	return view('siswa.ujian.selesaiUjian');
	}

    public function destroy($id)
    {
        $log_user = auth()->user()->username;
        $log = DB::table('test_log')
            ->where([
                ['code_test_log', $id],
                ['user_log', $log_user]
            ]);
        // dd($log);

        $log->delete();

        return redirect()->route('homeSiswa');
    }
    
}
