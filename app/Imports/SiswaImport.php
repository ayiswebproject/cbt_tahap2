<?php

namespace App\Imports;

use App\User;
use App\UserImport;
use App\Siswa;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SiswaImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection  $rows)
    {
        foreach ($rows as $row) 
        {
            UserImport::create([
                'id' => $row[0],
                'username' => $row[1],
                'email' => $row[2],
                'password' => bcrypt($row[3]),
                'role' => $row[4],
            ]);

            Siswa::create([
                'name' => $row[5],
                'gender' => $row[6],
                'no_induk' => $row[0],
                'class_id' => $row[7],
                'user_id' => $row[0],
            ]);
        }
    }
}
