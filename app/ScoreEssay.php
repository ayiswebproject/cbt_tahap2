<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreEssay extends Model
{
    protected $table = 'score_log';
}