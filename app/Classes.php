<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';

    protected $casts = ['id' => 'string'];

    public $incrementing = false;

    protected $fillable = ['class_id','class'];
}
