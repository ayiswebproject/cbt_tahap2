<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'students';

    protected $fillable = ['name','gender','class_id','user_id'];
}
