<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'teachers';

    protected $fillable = ['name','gender','subjects_id','user_id'];
}
