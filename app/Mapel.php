<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mapel extends Model
{
    protected $table = 'subjects';

    protected $casts = ['id' => 'string'];

    public $incrementing = false;

    protected $fillable = ['id','subjects'];
}
