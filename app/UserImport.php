<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserImport extends Model
{
    protected $table = 'users';

    protected $fillable = ['id','username','email', 'password', 'role'];
}
