<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Role Permission Admin by URL
Route::group(['middleware' => ['auth', 'admin']], function() {

    // Route Home
    Route::get('/haiAdmin', 'Admin\HomeAdminController@home')->name('homeAdmin');
    Route::get('/haiAdmin/fetchdataAkses', 'Admin\HomeAdminController@fetchdataAkses')->name('admin.fetchdataAkses');
    Route::post('/haiAdmin/updateAkses', 'Admin\HomeAdminController@updateAkses')->name('admin.updateAkses');

    // Route Kelas
    Route::get('/haiAdmin/kelas', 'Admin\ClassController@index')->name('kelas');
    Route::get('/haiAdmin/kelas/getData', 'Admin\ClassController@getData')->name('kelas.getData');
    Route::post('/haiAdmin/kelas/store', 'Admin\ClassController@store')->name('kelas.store');
    Route::get('/haiAdmin/kelas/fetchdata', 'Admin\ClassController@fetchdata')->name('kelas.fetchdata');
    Route::get('/haiAdmin/kelas/fetchedit', 'Admin\ClassController@fetchedit')->name('kelas.fetchedit');
    Route::get('/haiAdmin/kelas/delete', 'Admin\ClassController@delete')->name('kelas.delete');

    // Route Mapel
    Route::get('/haiAdmin/mapel', 'Admin\MapelController@index')->name('mapel');
    Route::get('/haiAdmin/mapel/getData', 'Admin\MapelController@getData')->name('mapel.getData');
    Route::post('/haiAdmin/mapel/store', 'Admin\MapelController@store')->name('mapel.store');
    Route::get('/haiAdmin/mapel/fetchdata', 'Admin\MapelController@fetchdata')->name('mapel.fetchdata');
    Route::get('/haiAdmin/mapel/fetchedit', 'Admin\MapelController@fetchedit')->name('mapel.fetchedit');
    Route::get('/haiAdmin/mapel/delete', 'Admin\MapelController@delete')->name('mapel.delete');

    // Route Guru
    Route::get('/haiAdmin/guru', 'Admin\GuruController@index')->name('guru');
    Route::get('/haiAdmin/guru/getData', 'Admin\GuruController@getData')->name('guru.getData');
    Route::get('/haiAdmin/guru/addForm', 'Admin\GuruController@addForm')->name('guru.addForm');
    Route::post('/haiAdmin/guru/storeAkses', 'Admin\GuruController@storeAkses')->name('guru.storeAkses');
    Route::get('/haiAdmin/guru/confirmSuccess', 'Admin\GuruController@confirmSuccess')->name('guru.confirmSuccess');
    Route::get('/haiAdmin/guru/formProfile/{id}', 'Admin\GuruController@formProfile')->name('guru.formProfile');
    Route::post('/haiAdmin/guru/storeProfile', 'Admin\GuruController@storeProfile')->name('guru.storeProfile');
    Route::post('/haiAdmin/guru/importExcel', 'Admin\GuruController@importExcel')->name('guru.importExcel');
    Route::get('/haiAdmin/guru/fetchdata', 'Admin\GuruController@fetchdata')->name('guru.fetchdata');
    Route::get('/haiAdmin/guru/fetchdataAkses', 'Admin\GuruController@fetchdataAkses')->name('guru.fetchdataAkses');
    Route::get('/haiAdmin/guru/fetchdataProfil', 'Admin\GuruController@fetchdataProfil')->name('guru.fetchdataProfil');
    Route::post('/haiAdmin/guru/updateAkses', 'Admin\GuruController@updateAkses')->name('guru.updateAkses');
    Route::post('/haiAdmin/guru/updateProfil', 'Admin\GuruController@updateProfil')->name('guru.updateProfil');
    Route::get('/haiAdmin/guru/delete', 'Admin\GuruController@delete')->name('guru.delete');

    // Route Siswa
    Route::get('/haiAdmin/siswa', 'Admin\SiswaController@index')->name('siswa');
    Route::get('/haiAdmin/siswa/getData', 'Admin\SiswaController@getData')->name('siswa.getData');
    Route::get('/haiAdmin/siswa/addForm', 'Admin\SiswaController@addForm')->name('siswa.addForm');
    Route::post('/haiAdmin/siswa/storeAkses', 'Admin\SiswaController@storeAkses')->name('siswa.storeAkses');
    Route::get('/haiAdmin/siswa/confirmSuccess', 'Admin\SiswaController@confirmSuccess')->name('siswa.confirmSuccess');
    Route::get('/haiAdmin/siswa/formProfile/{id}', 'Admin\SiswaController@formProfile')->name('siswa.formProfile');
    Route::post('/haiAdmin/siswa/storeProfile', 'Admin\SiswaController@storeProfile')->name('siswa.storeProfile');
    Route::post('/haiAdmin/siswa/importExcel', 'Admin\SiswaController@importExcel')->name('siswa.importExcel');
    Route::get('/haiAdmin/siswa/fetchdata', 'Admin\SiswaController@fetchdata')->name('siswa.fetchdata');
    Route::get('/haiAdmin/siswa/fetchdataAkses', 'Admin\SiswaController@fetchdataAkses')->name('siswa.fetchdataAkses');
    Route::get('/haiAdmin/siswa/fetchdataProfil', 'Admin\SiswaController@fetchdataProfil')->name('siswa.fetchdataProfil');
    Route::post('/haiAdmin/siswa/updateAkses', 'Admin\SiswaController@updateAkses')->name('siswa.updateAkses');
    Route::post('/haiAdmin/siswa/updateProfil', 'Admin\SiswaController@updateProfil')->name('siswa.updateProfil');
    Route::get('/haiAdmin/siswa/delete', 'Admin\SiswaController@delete')->name('siswa.delete');
    
    // Route Ujian & Soal
    Route::get('/haiAdmin/ujian', 'Admin\UjianController@index')->name('ujian');
    Route::get('/haiAdmin/ujianEssay', 'Admin\UjianController@indexEssay')->name('ujianEssay');
    Route::get('/haiAdmin/ujian/getData', 'Admin\UjianController@getData')->name('ujian.getData');
    Route::get('/haiAdmin/ujianEssay/getEssay', 'Admin\UjianController@getEssay')->name('ujianEssay.getEssay');
    Route::get('/haiAdmin/ujian/addForm', 'Admin\UjianController@addForm')->name('ujian.addForm');
    Route::post('/haiAdmin/ujian/store', 'Admin\UjianController@store')->name('ujian.store');
    Route::get('/haiAdmin/ujian/addSoal/{id}', 'Admin\UjianController@addSoal')->name('ujian.addSoal');
    Route::get('/haiAdmin/ujian/editDetailSoal/{id}', 'Admin\UjianController@editDetailSoal')->name('ujian.editDetailSoal');
    Route::post('/haiAdmin/ujian/storeSoal', 'Admin\UjianController@storeSoal')->name('ujian.storeSoal');
    Route::match(['put', 'patch'], '/haiAdmin/ujian/updateDetail/{id}','Admin\UjianController@updateDetail')->name('ujian.updateDetail');
    Route::match(['put', 'patch'], '/haiAdmin/ujian/updateDetailOnAdd/{id}','Admin\UjianController@updateDetailOnAdd')->name('ujian.updateDetailOnAdd');
    Route::get('/haiAdmin/ujian/fetchdataUjian', 'Admin\UjianController@fetchdataUjian')->name('ujian.fetchdataUjian');
    Route::get('/haiAdmin/ujian/fetchdataEditUjian', 'Admin\UjianController@fetchdataEditUjian')->name('ujian.fetchdataEditUjian');
    Route::post('/haiAdmin/ujian/updateUjian', 'Admin\UjianController@updateUjian')->name('ujian.updateUjian');
    Route::get('/haiAdmin/ujian/editSoal/{id}', 'Admin\UjianController@editSoal')->name('ujian.editSoal');
    Route::get('/haiAdmin/ujian/getKoreksiEssay/{id}', 'Admin\UjianController@getKoreksiEssay')->name('ujian.getKoreksiEssay');
    Route::get('/haiAdmin/ujian/koreksiEssay/{id}/{ids}', 'Admin\UjianController@koreksiEssay')->name('ujian.koreksiEssay');
    Route::match(['put', 'patch'], '/haiAdmin/ujian/addNilai/','Admin\UjianController@addNilai')->name('ujian.addNilai');
    Route::get('/haiAdmin/ujian/lihatHasil/{id}', 'Admin\UjianController@lihatHasil')->name('ujian.lihatHasil');
    Route::get('/haiAdmin/ujian/delete', 'Admin\UjianController@delete')->name('ujian.delete');
    Route::get('/haiAdmin/ujian/deleteLog', 'Admin\UjianController@deleteLog')->name('ujian.deleteLog');

    // Route Report
    Route::get('/haiAdmin/laporan', 'Admin\ReportController@index')->name('laporan');
    Route::post('/haiAdmin/downloadLaporan', 'Admin\ReportController@reportPDF')->name('downloadLaporan');

    Route::get('/isGuru', function(){
        return view('homeGuru');
    });

    // Route Home Siswa
    Route::get('/isSiswa', 'Siswa\HomeSiswaController@home')->name('homeSiswa');
    Route::get('/isSiswa/storeKodeSoal', 'Siswa\HomeSiswaController@storeKodeSoal')->name('storeKodeSoal');
    Route::get('/isSiswa/fetchdataAkses', 'Siswa\HomeSiswaController@fetchdataAkses')->name('fetchdataAkses');
    Route::post('/isSiswa/updateAkses', 'Siswa\HomeSiswaController@updateAkses')->name('updateAkses');

    // Route Ujian Siswa
    Route::get('/detailUjian/{id}', 'Siswa\ExamController@detailUjian')->name('detailUjian');
    Route::get('/ujian/{id}', 'Siswa\ExamController@ujian')->name('startUjian');
    Route::post('/ujian/storeJawaban', 'Siswa\ExamController@storeJawaban')->name('storeJawaban');
    Route::post('/ujian/storeJawabanEssay', 'Siswa\ExamController@storeJawabanEssay')->name('storeJawabanEssay');
    Route::get('/ujian/selesaiUjian', 'Siswa/ExamController@selesaiUjian')->name('selesaiUjian');

});

// Role Permission Guru by URL
Route::group(['middleware' => ['auth', 'guru']], function() {

    // Route Home
    Route::get('/haiGuru', 'Guru\HomeAdminController@home')->name('homeGuru');
    Route::get('/haiGuru/fetchdataAkses', 'Guru\HomeAdminController@fetchdataAkses')->name('homeGuru.fetchdataAkses');
    Route::post('/haiGuru/updateAkses', 'Guru\HomeAdminController@updateAkses')->name('homeGuru.updateAkses');

    // Route Kelas
    Route::get('/haiGuru/kelas', 'Guru\ClassController@index')->name('guru.kelas');
    Route::get('/haiGuru/kelas/getData', 'Guru\ClassController@getData')->name('guru.kelas.getData');
    Route::post('/haiGuru/kelas/store', 'Guru\ClassController@store')->name('guru.kelas.store');
    Route::get('/haiGuru/kelas/fetchdata', 'Guru\ClassController@fetchdata')->name('guru.kelas.fetchdata');
    Route::get('/haiGuru/kelas/fetchedit', 'Guru\ClassController@fetchedit')->name('guru.kelas.fetchedit');
    Route::get('/haiGuru/kelas/delete', 'Guru\ClassController@delete')->name('guru.kelas.delete');

    // Route Siswa
    Route::get('/haiGuru/siswa', 'Guru\SiswaController@index')->name('guru.siswa');
    Route::get('/haiGuru/siswa/getData', 'Guru\SiswaController@getData')->name('guru.siswa.getData');
    Route::get('/haiGuru/siswa/addForm', 'Guru\SiswaController@addForm')->name('guru.siswa.addForm');
    Route::post('/haiGuru/siswa/storeAkses', 'Guru\SiswaController@storeAkses')->name('guru.siswa.storeAkses');
    Route::get('/haiGuru/siswa/confirmSuccess', 'Guru\SiswaController@confirmSuccess')->name('guru.siswa.confirmSuccess');
    Route::get('/haiGuru/siswa/formProfile/{id}', 'Guru\SiswaController@formProfile')->name('guru.siswa.formProfile');
    Route::post('/haiGuru/siswa/storeProfile', 'Guru\SiswaController@storeProfile')->name('guru.siswa.storeProfile');
    Route::post('/haiGuru/siswa/importExcel', 'Admin\SiswaController@importExcel')->name('guru.siswa.importExcel');
    Route::get('/haiGuru/siswa/fetchdata', 'Guru\SiswaController@fetchdata')->name('guru.siswa.fetchdata');
    Route::get('/haiGuru/siswa/fetchdataAkses', 'Guru\SiswaController@fetchdataAkses')->name('guru.siswa.fetchdataAkses');
    Route::get('/haiGuru/siswa/fetchdataProfil', 'Guru\SiswaController@fetchdataProfil')->name('guru.siswa.fetchdataProfil');
    Route::post('/haiGuru/siswa/updateAkses', 'Guru\SiswaController@updateAkses')->name('guru.siswa.updateAkses');
    Route::post('/haiGuru/siswa/updateProfil', 'Guru\SiswaController@updateProfil')->name('guru.siswa.updateProfil');
    Route::get('/haiGuru/siswa/delete', 'Guru\SiswaController@delete')->name('guru.siswa.delete');
    
    // Route Ujian & Soal
    Route::get('/haiGuru/ujian', 'Guru\UjianController@index')->name('guru.ujian');
    Route::get('/haiGuru/ujianEssay', 'Guru\UjianController@indexEssay')->name('guru.ujianEssay');
    Route::get('/haiGuru/ujian/getData', 'Guru\UjianController@getData')->name('guru.ujian.getData');
    Route::get('/haiGuru/ujianEssay/getEssay', 'Guru\UjianController@getEssay')->name('guru.ujianEssay.getEssay');
    Route::get('/haiGuru/ujian/addForm', 'Guru\UjianController@addForm')->name('guru.ujian.addForm');
    Route::post('/haiGuru/ujian/store', 'Guru\UjianController@store')->name('guru.ujian.store');
    Route::get('/haiGuru/ujian/addSoal/{id}', 'Guru\UjianController@addSoal')->name('guru.ujian.addSoal');
    Route::get('/haiGuru/ujian/editDetailSoal/{id}', 'Guru\UjianController@editDetailSoal')->name('guru.ujian.editDetailSoal');
    Route::post('/haiGuru/ujian/storeSoal', 'Guru\UjianController@storeSoal')->name('guru.ujian.storeSoal');
    Route::match(['put', 'patch'], '/haiGuru/ujian/updateDetail/{id}','Guru\UjianController@updateDetail')->name('guru.ujian.updateDetail');
    Route::match(['put', 'patch'], '/haiGuru/ujian/updateDetailOnAdd/{id}','Guru\UjianController@updateDetailOnAdd')->name('guru.ujian.updateDetailOnAdd');
    Route::get('/haiGuru/ujian/fetchdataUjian', 'Guru\UjianController@fetchdataUjian')->name('guru.ujian.fetchdataUjian');
    Route::get('/haiGuru/ujian/fetchdataEditUjian', 'Guru\UjianController@fetchdataEditUjian')->name('guru.ujian.fetchdataEditUjian');
    Route::post('/haiGuru/ujian/updateUjian', 'Guru\UjianController@updateUjian')->name('guru.ujian.updateUjian');
    Route::get('/haiGuru/ujian/editSoal/{id}', 'Guru\UjianController@editSoal')->name('guru.ujian.editSoal');
    Route::get('/haiGuru/ujian/getKoreksiEssay/{id}', 'Guru\UjianController@getKoreksiEssay')->name('guru.ujian.getKoreksiEssay');
    Route::get('/haiGuru/ujian/koreksiEssay/{id}/{ids}', 'Guru\UjianController@koreksiEssay')->name('guru.ujian.koreksiEssay');
    Route::match(['put', 'patch'], '/haiGuru/ujian/addNilai/','Guru\UjianController@addNilai')->name('guru.ujian.addNilai');
    Route::get('/haiGuru/ujian/lihatHasil/{id}', 'Guru\UjianController@lihatHasil')->name('guru.ujian.lihatHasil');
    Route::get('/haiGuru/ujian/delete', 'Guru\UjianController@delete')->name('guru.ujian.delete');

    // Route Report
    Route::get('/haiGuru/laporan', 'Admin\ReportController@index')->name('guru.laporan');
    Route::post('/haiGuru/downloadLaporan', 'Admin\ReportController@reportPDF')->name('guru.downloadLaporan');

    Route::get('/siswa', function(){
        return view('homeSiswa');
    });

});

// Role Permission Siswa by URL
Route::group(['middleware' => ['auth', 'siswa']], function() {

    // Route Home
    Route::get('/', 'Siswa\HomeSiswaController@home')->name('homeSiswa');
    Route::get('/storeKodeSoal', 'Siswa\HomeSiswaController@storeKodeSoal')->name('storeKodeSoal');
    Route::get('/fetchdataAkses', 'Siswa\HomeSiswaController@fetchdataAkses')->name('fetchdataAkses');
    Route::post('/updateAkses', 'Siswa\HomeSiswaController@updateAkses')->name('updateAkses');

    // Route Ujian Siswa
    Route::get('/detailUjian/{id}', 'Siswa\ExamController@detailUjian')->name('detailUjian');
    Route::get('/detailUjian/destroy/{id}', 'Siswa\ExamController@destroy')->name('detail.destroy');
    Route::get('/ujian/{id}', 'Siswa\ExamController@ujian')->name('startUjian');
    Route::post('/ujian/storeJawaban', 'Siswa\ExamController@storeJawaban')->name('storeJawaban');
    Route::post('/ujian/storeJawabanEssay', 'Siswa\ExamController@storeJawabanEssay')->name('storeJawabanEssay');
    Route::get('/ujian/selesaiUjian', 'Siswa/ExamController@selesaiUjian')->name('selesaiUjian');

});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');