@extends('admin.layouts.header-footer')

@section('css-app')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Pelajaran</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Pelajaran</h6>
            <button type="button" id="add" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mapel_modal">
                <i class="fas fa-plus"></i>
                Tambah
            </button>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <span id="form_output"></span>
            <table class="table table-bordered" id="mapel_table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>ID Pelajaran</th>
                        <th>Mata Pelajaran</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    <div id="mapel_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="add_mapel_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Data Mapel</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>ID Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="subjects_id" name="subjects_id" class="form-control" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="subjects" name="subjects" class="form-control" autofocus>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="insert">
                        <input type="submit" name="submit" id="action" value="Tambah" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/master/subjects/subjects.js') }}"></script>
    <script src="{{ asset('js/master/subjects/subjects-table.js') }}"></script>
    <script src="{{ asset('js/master/subjects/subjects-ajax.js') }}"></script>
@stop
