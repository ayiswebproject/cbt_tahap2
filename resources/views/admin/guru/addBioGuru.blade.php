@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    ul.sidebar-dark {
        display: none !important;
    }
    .form-group {
        margin-bottom: 25px;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Lengkapi Profil Guru</h1>
                <br>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Profil Guru</h6>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/haiAdmin/guru/storeProfile') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Nama Guru<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" placeholder="Nama Guru" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="img_profil" value="No Profile Image">
                                    @foreach($guru as $gurus)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>No. Induk Guru<span style="color: red">*</span></label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text" value="{{ $gurus->id }}" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Jenis Kelamin<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="gender" required>
                                                    <option value="" selected>Pilih</option>
                                                    <option value="Laki-laki">Laki-laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Pelajaran<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="subjects_id" required>
                                                    <option value="" selected>Pilih</option>
                                                    @foreach($mapel as $mapels)
                                                        <option value="{{ $mapels->id }}">{{ $mapels->subjects }}</option>
                                                    @endforeach
                                                </select>
                                                @foreach($guru as $gurus)
                                                    <input type="hidden" name="user_id" class="form-control" value="{{ $gurus->id }}">
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            
                            
                            <div class="btn-group">
                                <button class="btn btn-primary">
                                    <i class="fas fa-check"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
