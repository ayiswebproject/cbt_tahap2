@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    ul.sidebar-dark {
        display: none !important;
    }
    #main-content h1 {
        text-align: center;
        font-style: italic;
        font-size: 72pt;
    }
    @media (max-width: 600px) {
        #main-content h1 {
            font-size: 42px;
        }
    }
    #main-content h5 {
        text-align: center;
        font-size: 18pt;
    }
    @media (max-width: 600px) {
        #main-content h5 {
            font-size: 16px;
        }
    }
    #main-content .left {
        float: right;
    }
    .col-sm-6 {
        margin-bottom: 25px;
    }
    .brand-card {
        padding: 35px 35px;
    }
    @media (max-width: 600px) {
        .brand-card {
            padding: 0px 0px;
        }
    }
    .confirm h5 {
        font-size: 20px !important;
    }
    .confirm img {
        width: 20%;
        display: block;
        margin: 15px auto;
    }
    .confirm {
        padding: 60px 0;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Berhasil Registrasi</h1>
                <br>
                <div class="row justify-content-center" id="main-content">
                  <div class="col-sm-6">
                      <div class="card">
                          <div class="card-body confirm">
                            <img src="{{ asset('img/checked.png') }}">
                              <h5>Akses User Guru telah terdaftar</h5>
                              <br>
                                @foreach($guru as $gurus)
                                    <a href="{{ url('/haiAdmin/guru/formProfile', $gurus->id) }}" style="text-align: center; display: block;">
                                        <button class="btn btn-primary">
                                            Lengkapi Profil Guru
                                            <i class="fas fa-arrow-right"></i>
                                        </button>
                                    </a>
                                @endforeach
                          </div>
                      </div>
                  </div>
                </div>

                                
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
    <br><br><br>
    <br><br><br>
    <br><br>
@stop

@section('js-app')

@stop
