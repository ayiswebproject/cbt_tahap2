@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    .bg-nav-soal {
        background-color: #1CC88A;
        border-radius: 50%;
        padding: 10px 8px 10px 8px;
        box-shadow: 1px 1px 5px #b4b4b4;
        display: block;
        margin: 0 auto;
        margin-bottom: 30px;
        color: #fff;
    }
    .bg-nav-soal span {
        text-align: center;
        display: block;
    }
    .bg-nav-soal:hover {
        background-color: #1CC88A;
        color: #fff;
    }
    .active .bg-nav-soal {
        background-color: #1CC88A !important;
        color: #fff !important; 
    }
    .nav-keterangan {
        margin-top: 20px;
    }
    .nav-keterangan .badge {
        margin-bottom: 7px;
        margin-right: 7px;
    }
    .nav-keterangan .badge-light {
        background-color: #f0f0f0;
    }
    .nav-keterangan .badge-success {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 11px 10px 11px;
        border-radius: 50%;
    }
    .nav-keterangan .badge-light {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 13px 10px 13px;
        border-radius: 50%;
    }
    .card-header a.btn,
    .card-header button {
        float: right;
        margin-top: -20px;
        color: #fff;
    }
    .numbering {
        vertical-align: text-top;
    }
    .questions .form-check {
        padding: 10px 25px;
    }
    .carousel-indicators {
        position: relative;
        margin-right: 7%;
        margin-left: 7%;
    }
    .carousel-indicators span {
        width: 30px;
    }
    form label {
        font-weight: bold;
    }
    ul.sidebar-dark {
        display: none !important;
    }
    .btn-info {
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
        cursor: pointer;
        color: #fff !important;
    }
    .carousel-inner td {
        font-size: 14px;
    }
    .answer label {
        font-weight: bold;
        font-size: 14px;
    }
    .answer span {
        font-size: 14px;
    }
    span#key {
        text-transform: uppercase;
    }
    .carousel-item td > h5 {
        font-weight: bold;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Koreksi Essay</h1>
                <br>

                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                    </div>
                @endif
                @if(Session::has('alert-info'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                    </div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                    </div>
                @endif

                <!-- DataTales Example -->
                <div id="soalNavigation" class="carousel slide row" data-ride="carousel" data-interval="false">
                    <div class="col-sm-4">
                        <div class="card shadow mb-4" id="data_soal">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Navigasi Soal</h6>
                                @foreach($check as $hiding)
                                    @if($hiding->results != null)
                                        <span class="text-primary score-text" style="float: right; margin-top: -25px; font-size: 25px;"><b>Nilai</b> : {{ number_format($hiding->results, 2, '.', ',') }}</span>
                                    @else
                                        
                                    @endif
                                @endforeach
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: auto; height: 325px;">
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                        <div class="row carousel-indicators">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach($check as $gets)
                                                <div class="col-sm-3 {{ $loop->first ? 'active' : '' }}">
                                                    <a data-target="#soalNavigation" data-slide-to="{{ $loop->index }}">
                                                        <button class="btn bg-nav-soal">
                                                            <span>{{ $i++ }}</span>
                                                        </button>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="nav-keterangan">
                                    <div class="ket-done">
                                        <span class="badge badge-success">
                                            <i class="fas fa-check"></i>
                                        </span> Soal Sudah Dijawab
                                    </div>
                                    <div class="ket-false">
                                        <span class="badge badge-light">
                                            <i class="fas fa-times"></i>
                                        </span> Soal Belum Dijawab
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Soal Ujian</h6>
                                <a href="{{ route('ujianEssay') }}">
                                    <button class="btn btn-secondary btn-sm">
                                        <i class="fas fa-arrow-left"></i>
                                        Kembali
                                    </button>
                                </a>
                            </div>
                            <div class="card-body">
                            @foreach($test_id as $tid)
                                <form method="POST" action="{{ route('ujian.addNilai', $tid->id) }}">
                            @endforeach
                                @foreach($check as $gets)
                                    <input type="hidden" id="test_id" name="test_id" value="{{ $gets->test_id }}" class="form-control" autofocus>
                                    <input type="hidden" id="user_id" name="user_id" value="{{ $gets->user_id }}" class="form-control" autofocus>
                                @endforeach
                                
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                      <div class="carousel-inner">
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach($check as $gets)
                                            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                                <table width="100%" id="soal-list">
                                                    <tr>
                                                        
                                                        <td class="numbering" width="7%">
                                                            <h5>{{ $no }}.</h5>
                                                        </td>
                                                        <td>
                                                            <span id="questions_view">{{ $gets->questions }}</span>
                                                            <br><br>
                                                            @if( $gets->quest_img != null )
                                                                <img src="{{ asset('uploads/soal/'.$gets->quest_img) }}" style="width: 20%"><br><br>
                                                            @else

                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <div class="answer">
                                                                <table class="table table-bordered" width="100%">
                                                                    <tr>
                                                                        <td style="width: 20%">
                                                                            <label>Kunci Jawaban</label>
                                                                        </td>
                                                                        <td style="vertical-align: text-top;">
                                                                            <span id="key_essay">{{ $gets->answer_essay }}</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <label>Hasil Jawaban</label>
                                                                        </td>
                                                                        <td style="vertical-align: text-top;">
                                                                            <span id="key_essay">{{ $gets->choice_essay }}</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <label>Dikerjakan oleh</label>
                                                                        </td>
                                                                        <td style="vertical-align: text-top;">
                                                                            <span id="key_essay">{{ $gets->name }}</span>
                                                                        </td>
                                                                    </tr>
                                                                    @if($gets->results == null)
                                                                        <tr>
                                                                            <td>
                                                                                <label>Skor / Nilai</label>
                                                                            </td>
                                                                            <td style="vertical-align: text-top;">
                                                                                <input type="hidden" name="answer_id[]" value="{{ $gets->id }}">
                                                                                <input type="number" name="score[]" id="score" class="form-control" style="width: 25%;" required>
                                                                            </td>
                                                                        </tr>
                                                                    @else

                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <br>
                                                <div class="dropdown-divider"></div>
                                            </div>
                                            @php
                                                $no++
                                            @endphp
                                        @endforeach
                                      </div>
                                        @if($gets->results == null)
                                            <div style="float: right; margin-top: 5px;">
                                                <button class="btn btn-success">
                                                    <i class="fas fa-check"></i>
                                                    Input Nilai
                                                </button>
                                            </div>
                                            <br>
                                        @else

                                        @endif
                                        <br>
                                        <div style="margin-top: 5px;">
                                            <a href="#soalNavigation" data-slide="prev" class="btn btn-secondary" style="float: left;">
                                                <i class="fas fa-arrow-left"></i>
                                                Sebelumnya
                                            </a>
                                            <a href="#soalNavigation" data-slide="next" class="btn btn-primary" style="float: right;">
                                                Selanjutnya
                                                <i class="fas fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <div id="input_nilai" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                @foreach($check as $input)
                    <form method="POST" action="{{ route('ujian.addNilai', $input->user_id) }}" id="input_form">
                        <div class="modal-header">
                            <h4 class="modal-title">Input Nilai</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <span id="error_output"></span>
                            <div class="form-group">
                                <label>Nilai<span style="color:red;">*</span></label>
                                <input type="text" id="results" name="results" class="form-control" autofocus>
                                <input type="hidden" id="test_id" name="test_id" value="{{ $input->test_id }}" class="form-control" autofocus>
                                <input type="hidden" id="user_id" name="user_id" value="{{ $input->user_id }}" class="form-control" autofocus>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary btn-sm">Submit</button>
                            <a style="cursor: pointer; color: #fff;" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</a>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function () {

        $('#selesai').click(function() {
            var count = 0;
            $('textarea').each(function(){
                if ($(this).val() == "") {
                    alert('gagal');
                    return false;
                }
                
            });
        });


        $('form #test_id ~ #test_id, form #user_id ~ #user_id, .card-header .text-primary.score-text ~ .score-text').remove();

    });
</script>
@stop
