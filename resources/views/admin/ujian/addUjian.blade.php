@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    .form-group {
        margin-bottom: 25px;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Tambah Data Ujian</h1>
                <br>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Tambah Data Ujian</h6>
                        <a href="{{ url('/haiAdmin/ujian') }}">
                            <button class="btn btn-secondary btn-sm">
                                <i class="fas fa-arrow-left"></i>
                                Kembali
                            </button>
                        </a>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/haiAdmin/ujian/store') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="code_test">Kode Ujian<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <input id="code_test" type="text" class="form-control" name="code_test" placeholder="Kode Ujian" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="name_test">Nama Ujian<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <input id="name_test" type="text" class="form-control" name="name_test" placeholder="Nama Ujian" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="duration">Durasi (Menit)<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <input id="duration" type="number" class="form-control" name="duration" placeholder="Durasi (Menit)" required>
                                            </div>
                                        </div>
                                                
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="type_test">Tipe Ujian<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select id="type_test" name="type_test" class="form-control" required>
                                                    <option selected value="">Pilih</option>
                                                    <option value="Pilihan Ganda">Pilihan Ganda</option>
                                                    <option value="Essay/Uraian">Essay/Uraian</option>
                                                </select>
                                            </div>
                                        </div>
                                                
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Pelajaran<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="subjects_id" required>
                                                    <option value="" selected>Pilih</option>
                                                    @foreach($mapel as $mapels)
                                                        <option value="{{ $mapels->id }}">{{ $mapels->subjects }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>     
                                    </div>
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                </div>
                            </div>

                            <div class="btn-group">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fas fa-check"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
