@extends('admin.layouts.header-footer')

@section('css-app')
<style type="text/css">
    ul.sidebar-dark {
        display: none !important;
    }
    .container-fluid.container-soal {
        margin-top: 25px;
        margin-bottom: 50px;
    }
    .btn-group .btn {
        border-top-left-radius: 0.35rem !important;
        border-bottom-left-radius: 0.35rem !important;
        border-top-right-radius: 0.35rem !important;
        border-bottom-right-radius: 0.35rem !important;
        margin-top: 15px;
    }
    .table td {
        font-size: 14px !important;
        vertical-align: baseline;
    }
    .form-updateDetail .form-control {
        font-size: 12px;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
    document.onkeydown = function() {    
        switch (event.keyCode) { 
            case 116 : //F5 button
                event.returnValue = false;
                event.keyCode = 0;
                return false; 
            case 82 : //R button
                if (event.ctrlKey) { 
                    event.returnValue = false; 
                    event.keyCode = 0;  
                    return false; 
                } 
        }
    }
</script>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid container-soal">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 mb-4">Edit Soal Ujian</h1>

        <div class="row">
            <div class="col-sm-4">
                <div class="card" style="box-shadow: 1px 1px 10px rgba(58, 59, 69, 0.15); margin-top: 5px;">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Detail Ujian</h6>
                    </div>
                    <div class="card-body">
                        @foreach($check as $details)
                            <form class="form-updateDetail">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <table class="table table-borderless">
                                    <tr>
                                        <td style="font-weight: bold;">Kode Ujian</td>
                                        <td>
                                            <input type="text" name="code_test" class="form-control" value="{{ $details->code_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Nama Ujian</td>
                                        <td>
                                            <input type="text" name="name_test" class="form-control" value="{{ $details->name_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Durasi</td>
                                        <td>
                                            <input type="text" name="duration" class="form-control" value="{{ $details->duration }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Type Ujian</td>
                                        <td>
                                            <input type="text" name="type_test" class="form-control" value="{{ $details->type_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Nama Mapel</td>
                                        <td>
                                            <input type="text" name="subjects_id" class="form-control" value="{{ $details->subjects }}" disabled>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Jumlah Soal</td>
                                        <td>
                                            @if($details->jml_soal == null)
                                                <input type="text" name="jml_soal" class="form-control" id="jml_soal" value="" readonly>
                                            @else
                                                <input type="text" name="jml_soal" class="form-control" id="soal_jml" value="" readonly>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-group">
                                    <a href="{{ route('ujian.editSoal', $details->test_id) }}">
                                        <span class="btn btn-primary btn-sm">
                                            <i class="fa fa-check"></i>
                                            Selesai
                                        </span>
                                    </a>
                                </div>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
            @foreach($check as $datass)
                <form id="add_soal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="card" style="box-shadow: 1px 1px 10px rgba(58, 59, 69, 0.15); margin-top: 5px;">
                        <div class="card-header">
                            <h6 class="m-0 font-weight-bold text-primary">Form Soal</h6>
                            <input type="hidden" name="test_id" id="test_id" value="{{ $datass->test_id }}">
                        </div>
                        <div class="card-body">

                            <span id="form_output"></span>
                            <span id="error_output"></span>

                            <div class="form-group">
                                <label id="soal">Soal Ujian<span style="color: red">*</span></label>
                                <textarea for="soal" class="form-control" name="questions" rows="5" value="" id="questions" required>{{ $datass->questions }}</textarea>
                            </div>
                            <div class="form-group">
                                @if($datass->quest_img == null)
                                <img src="{{ url('uploads/soal/', $datass->quest_img) }}" id="img-show" style="width: 15%; margin-bottom: 15px; display: none;"><br>
                                <input type="file" name="quest_img" id="quest_img">
                                @else
                                <img src="{{ url('uploads/soal/', $datass->quest_img) }}" id="img-show" style="width: 15%; margin-bottom: 15px;"><br>
                                <input type="file" name="quest_img" id="quest_img">
                                @endif
                            </div>
                            @foreach($check as $value)
                            @if($value->type_test == 'Pilihan Ganda')
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label id="a_field">Pilihan A<span style="color: red">*</span></label>
                                            <textarea for="a_field" class="form-control" name="a" value="" id="a" required>{{ $datass->a }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="b_field">Pilihan B<span style="color: red">*</span></label>
                                            <textarea for="b_field" class="form-control" name="b" value="" id="b" required>{{ $datass->b }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="c_field">Pilihan C<span style="color: red">*</span></label>
                                            <textarea for="c_field" class="form-control" name="c" value="" id="c" required>{{ $datass->c }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label id="d_field">Pilihan D<span style="color: red">*</span></label>
                                            <textarea for="d_field" class="form-control" name="d" value="" id="d" required>{{ $datass->d }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="e_field">Pilihan E<span style="color: red">*</span></label>
                                            <textarea for="e_field" class="form-control" name="e" value="" id="e" required>{{ $datass->e }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                            <label id="answer_field_key">Kunci Jawaban<span style="color: red">*</span> : <span id="key_preview" style="text-transform: uppercase;">{{ $datass->answer_key }}</span></label>
                                            <!-- <input for="answer_field_key" type="text" class="form-control" name="answer_key" id="answer_key" value="{{ $datass->answer_key }}" required> -->
                                            <table width="75%">
                                                <input type="hidden" name="answer_key" id="key_hidden" value="{{ $datass->answer_key }}">
                                                <tr>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_a" value="a">
                                                        <label for="key_a" style="font-weight: 400;">A</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_b" value="b">
                                                        <label for="key_b" style="font-weight: 400;">B</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_c" value="c">
                                                        <label for="key_c" style="font-weight: 400;">C</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_d" value="d">
                                                        <label for="key_d" style="font-weight: 400;">D</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_e" value="e">
                                                        <label for="key_e" style="font-weight: 400;">E</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div> 
                                    </div>
                                </div>
                            @elseif($value->type_test == 'Essay/Uraian')
                                <div class="form-group">
                                    <label id="answer_essay_key">Kunci Jawaban<span style="color: red">*</span></label>
                                    <textarea for="answer_essay_key" class="form-control" name="answer_essay" value="" id="answer_essay" required>{{ $datass->answer_essay }}</textarea>
                                </div>
                            @endif
                            @endforeach
                            <div class="btn-group">
                                <input type="hidden" name="id" id="id" value="{{ $datass->id }}">
                                <input type="hidden" name="button_action" id="button_action" value="update">
                                <input type="submit" name="submit" id="action" value="Edit Soal" class="btn btn-primary btn-sm">
                                <!-- <a href="{{ route('ujian') }}" class="btn btn-success" style="margin-left: 10px;">
                                    <i class="fa fa-check"></i>
                                    Selesai
                                </a> -->
                            </div>
                        </div>
                    </div>
                </form>
            @endforeach
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type=radio]').click(function() {
            $('#key_hidden').remove();
        });

        var soal_jml = '{{ $details->jml_soal }}';
        // $('.btn.btn-success').hide();
        $('#jml_soal').val(0);
        $('#soal_jml').val('{{ $details->jml_soal }}');
        $('#add_soal').on('submit', function(event) {
            event.preventDefault();
            
            $.ajax({
                url:"{{ route('ujian.storeSoal') }}",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType: 'json',
                success:function(data)
                {
                    if (data.error.length > 0) {
                        var error_html = '';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                        }
                        $('#error_output').html(error_html);
                        $('#error_output').show();
                    } else {
                        $('#form_output').html(data.success);
                        $('#add_soal')[0].reset();
                        $('#soal_jml').val(soal_jml);
                        $('#button_action').val('update');
                        $('#form_output').show();
                        $('#error_output').hide();
                        $('#questions').val(data.hasil.questions);
                        $('#a').val(data.hasil.a);
                        $('#b').val(data.hasil.b);
                        $('#c').val(data.hasil.c);
                        $('#d').val(data.hasil.d);
                        $('#e').val(data.hasil.e);
                        $('#key_preview').html(data.hasil.answer_key);
                        $('#answer_essay').val(data.hasil.answer_essay);
                        $('#img-show').attr('src', '{{ url("uploads/soal") }}/' + data.hasil.quest_img );
                        if (data.hasil.quest_img == null) {
                            $('#img-show').hide();
                        } else {
                            $('#img-show').show();
                        }
                        // $('.btn.btn-success').show();
                    }
                }
            })
        });
    });
</script>
@stop
