@extends('admin.layouts.header-footer')

@section('css-app')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Hasil Ujian</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Hasil Ujian</h6>
          <a href="{{ url('/haiAdmin/ujianEssay') }}">
              <button class="btn btn-secondary btn-sm">
                <i class="fas fa-arrow-left"></i>
                Kembali
              </button>
          </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="koreksi_essay" width="100%" cellspacing="0">
                <thead>
                  <tr>
                      <th>Kode Ujian</th>
                      <th>Nama Ujian</th>
                      <th>Tipe Ujian</th>
                      <th>Pelajaran</th>
                      <th>Dikerjakan Oleh</th>
                      <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($essay as $uraian)
                    <tr>
                        <td>{{ $uraian->code_test }}</td>
                        <td>{{ $uraian->name_test }}</td>
                        <td>{{ $uraian->type_test }}</td>
                        <td>{{ $uraian->subjects }}</td>
                        <td>{{ $uraian->name }}</td>
                        <td>
                          @if($uraian->results == null)
                            <a href="{{ route('ujian.koreksiEssay', ['id' => $uraian->id, 'ids' => $uraian->user_id]) }}" class='btn btn-sm btn-outline-success see-soal' title='Koreksi'>
                              <i class='fa fa-check-double'></i> Koreksi
                            </a>
                          @else
                            <span class="badge badge-success" style="font-size: 12px;">Selesai</span>
                          @endif
                        </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#koreksi_essay').DataTable();
        });
    </script>
@stop
