@extends('siswa.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card img {
            width: 55%;
            margin: 0 auto;
            display: block;
            padding-bottom: 25px;
        }
        .brand-card {
            padding: 20px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .profil-identity label {
            text-align: center; 
            display: block; 
            font-size: 12px; 
            color: #fff; 
            font-weight: 600;
        }
        .modal-dialog {
            max-width: 550px;
        }
    </style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Home</h1>
        
      <div class="row" id="main-content">
          <div class="col-sm-6">
              <div class="card shadow h-100">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Welcome, {{ Auth::user()->name }}</h6>
                </div>
                <div class="card-body">
                    <div class="brand-card">
                        <img src="{{ asset('img/ayocbt_dark.png') }}">
                        <a href="#" style="text-align: center; display: block;" data-toggle='modal' data-target='#startexam_modal'>
                            <button class="btn btn-primary">
                                Mulai Ujian
                            </button>
                        </a>
                    </div>
                </div>
              </div>
          </div>
          <div class="col-sm-6">
                <div class="card shadow h-100">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Profil Saya</h6>
                    </div>
                    @if(Auth::user()->role == 'admin')
                        <a href="{{ route('homeAdmin') }}" style="display: block;text-align: center;padding: 9rem 0 0 0;">
                            <button class="btn btn-secondary">
                                <i class="fas fa-arrow-left"></i>
                                Kembali ke Dashboard Admin
                            </button>
                        </a>
                    @elseif(Auth::user()->role == 'guru')

                    @elseif(Auth::user()->role == 'siswa')
                        <div class="card-body">
                            <span id="form_output"></span>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="card" style="background-color: #4E73DF">
                                        <div class="card-body">
                                            <img src="{{ asset('img/user.png') }}" class="img-profile rounded-circle" style="width: 50% !important;">
                                            <br>
                                            @foreach($siswa as $siswas)
                                                <div class="profil-identity">
                                                    <label>{{ $siswas->name }}</label>
                                                    <label>{{ $siswas->id }}</label>
                                                    <label>{{ $siswas->class }}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="home-profile">
                                        <div class="card">
                                            @foreach($siswa as $profil)
                                                <div class="card-body">
                                                    <div class="row" style="margin-bottom: 15px;">
                                                        <div class="col-sm-3">
                                                            <label class="control-label bold">Email</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input class="form-control" id="email_show" value="{{ $profil->email }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 15px;">
                                                        <div class="col-sm-3">
                                                            <label class="control-label bold">Username</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input class="form-control" id="username_show" value="{{ $profil->username }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 15px;">
                                                        <div class="col-sm-3">
                                                            <label class="control-label bold">Gender</label>
                                                        </div>
                                                        <div class="col-sm-9">
                                                            <input class="form-control" id="gender_show" value="{{ $profil->gender }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="left">
                                <a href="#" class="btn btn-outline-secondary btn-sm edit-akses" data-toggle="modal" data-target="#password_modal" id="{{ Auth::user()->id }}">
                                    <i class="fas fa-unlock-alt"></i>
                                    &nbsp;Ganti Password
                                </a>
                                <!-- <a href="#" class="btn btn-primary btn-icon-split btn-sm">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Edit Profil</span>
                                </a> -->
                            </div>
                        </div>
                    @endif
                </div>
          </div>
      </div>

    </div>
    <!-- /.container-fluid -->
    <div style="margin-top: 80px"></div>

    <div id="startexam_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="GET" action="{{ route('storeKodeSoal') }}" id="startexam_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Masukkan Kode Ujian</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- <span id="error_output"></span> -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Kode Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="hidden" id="user_log" name="user_log" class="form-control" value="{{ Auth::user()->username }}">
                                    <input type="text" id="code_test_log" name="code_test_log" class="form-control" autofocus>
                                </div>
                            </div>
                                    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-sm">Submit</button>
                        <a class="btn btn-secondary btn-sm" data-dismiss="modal" style="color: #fff;">Tutup</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="password_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="password_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Ganti Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_password"></span>
                        <div id="akses_bio">
                                <input type="hidden" id="idAkses" name="id" class="form-control">
                                <input type="hidden" id="emailAkses" name="email" class="form-control" readonly>
                                <input type="hidden" id="usernameAkses" name="username" class="form-control">
                        </div>
                        <div id="akses_pswd">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password" type="password" class="form-control" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Konfirmasi Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password-confirm" type="password" class="form-control pswd-conf" name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="role" class="form-control" value="siswa">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Submit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function() {
        $('.close, .close-btn').click(function() {
            $('#error_password').hide();
        });

        $('#akses_pswd').show();

        $('#password_form').on('submit', function(event) {
            event.preventDefault();

            var form_data = $(this).serialize();

            $.ajax({
                url:"{{ route('updateAkses') }}",
                method:"POST",
                data:form_data,
                dataType:"json",
                success:function(data)
                {
                    if (data.error.length > 0) {
                        var error_html = '';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                        }
                        $('#error_password').html(error_html);
                        $('#password').val('');
                        $('#password-confirm').val('');
                        $('#error_password').show();
                    } else {
                        $('#form_output').html(data.success);
                        $('#password_form')[0].reset();
                        $('#button_action').val('update');
                        $('#password_modal').modal('hide');
                        $('#form_output').show();
                        $('#error_password').hide();
                        $('#action').show();
                    }
                }
            })
        });

        $(document).on('click', '.edit-akses', function() {
            var id = $(this).attr('id');
            $.ajax({
                url:"{{ route('fetchdataAkses') }}",
                method:"GET",
                data:{id:id},
                dataType:"json",
                success:function(data)
                {
                    $('#idAkses').val(data.id);
                    $('#emailAkses').val(data.email);
                    $('#usernameAkses').val(data.username);
                    $('#action').val('Submit');
                    $('.modal-title').text('Ganti Password');
                    $('#id').val(id);
                    $('#form_output').hide();
                    $('#error_output').show();
                }
            })
        });
    });
</script>
@stop
