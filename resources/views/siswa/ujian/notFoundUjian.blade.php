@extends('siswa.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card {
            padding: 35px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .btn-nav {
            padding: 200px 260px;
        }
        .btn-back {
            float: left;
        }
        .btn-next {
            float: right;
        }
        .brand-card {
            padding: 88px 125px;
        }
        #start_exam_card .back {
            float: right;
            display: block;
        }
        #start_exam_card .next {
            float: left;
        }
    </style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Detail Ujian</h1>

      <div class="row" id="main-content">
            <style type="text/css">
                #warning {
                    display: none;
                }
            </style>
            <div class="col-sm-6">
                <div class="card shadow">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Data Ujian</h6>
                    </div>
                    <div class="card-body">
                        <div class="brand-card">
                            <h2 class="text-center">Kode ujian tidak ditemukan</h2>
                            <br>
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <a href="{{ route('homeSiswa') }}" class="back btn btn-secondary col-sm-12">
                                        <i class="fa fa-arrow-left"></i>
                                        &nbsp;Kembali
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
