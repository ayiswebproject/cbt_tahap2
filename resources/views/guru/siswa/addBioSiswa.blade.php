@extends('guru.layouts.header-footer')

@section('css-app')
<style type="text/css">
    ul.sidebar-dark {
        display: none !important;
    }
    .form-group {
        margin-bottom: 25px;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
</script>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Lengkapi Profil Siswa</h1>
                <br>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Profil Siswa</h6>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ url('/haiGuru/siswa/storeProfile') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Nama Siswa<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" placeholder="Nama Siswa" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="img_profil" value="No Profile Image">
                                    @foreach($siswa as $siswas)
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>No. Induk Siswa<span style="color: red">*</span></label>
                                                </div>
                                                <div class="col-sm-9">
                                                    <input type="text" value="{{ $siswas->id }}" class="form-control" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Jenis Kelamin<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="gender" required>
                                                    <option value="" selected>Pilih</option>
                                                    <option value="Laki-laki">Laki-laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Kelas<span style="color: red">*</span></label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="class_id" required>
                                                    <option value="" selected>Pilih</option>
                                                    @foreach($class as $classes)
                                                        <option value="{{ $classes->id }}">{{ $classes->class }}</option>
                                                    @endforeach
                                                </select>
                                                @foreach($siswa as $siswas)
                                                    <input type="hidden" name="user_id" class="form-control" value="{{ $siswas->id }}">
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="btn-group">
                                <button class="btn btn-primary">
                                    <i class="fas fa-check"></i>
                                    Simpan
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
