@extends('guru.layouts.header-footer')

@section('css-app')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<style type="text/css">
    .form-group {
        margin-bottom: 25px;
    }
    .modal-dialog {
        max-width: 550px;
    }
</style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Siswa</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Siswa</h6>
          <a href="{{ url('/haiGuru/siswa/addForm') }}">
              <button class="btn btn-primary btn-sm">
                <i class="fas fa-user"></i>
                Registrasi User
              </button>
          </a>
          <a href="javascript:void(0);" data-toggle="modal" data-target="#importModal">
            <button class="btn btn-success btn-sm" style="margin-right: 5px;">
                <i class="fas fa-file-excel"></i>
                Import Excel
            </button>
          </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                </div>
            @endif
            @if(Session::has('alert-info'))
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                </div>
            @endif
            @if(Session::has('alert-danger'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                </div>
            @endif
            <span id="form_output"></span>
            <table class="table table-bordered" id="siswa_table" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No. Induk</th>
                        <th>Nama Siswa</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Gender</th>
                        <th>Kelas</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    <div id="detail_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="detail_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail Profil Siswa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>No. Induk<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="idd" name="id" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nama Siswa<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="name" name="name" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Email<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="email" id="email" name="email" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Username<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="username" name="username" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Jenis Kelamin<span style="color:red;">*</span></label>
                                    
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="gender" name="gender" class="form-control" readonly>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Kelas<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" id="class" name="class" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="akses_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="akses_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Akses User</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div id="akses_bio">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>No. Induk<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" id="ids" name="ids" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Email<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="email" id="emailAkses" name="email" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Username<span style="color:red;">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" id="usernameAkses" name="username" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="akses_pswd">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password" type="password" class="form-control" name="password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Konfirmasi Password<span style="color: red">*</span></label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input id="password-confirm" type="password" class="form-control pswd-conf" name="password_confirmation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="margin-left: 5px;">
                            <input type="checkbox" class="check-pass" name="check" value="">
                            <label for="check_pswd"><b>Ganti Password Anda?</b></label>
                        </div>
                        <input type="hidden" name="role" class="form-control" value="guru">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="profil_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="profil_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Profil Siswa</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_profil"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Nama Siswa</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" name="nameSiswa" class="form-control" id="nameAkses">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="hidden" name="id" id="id_prof">
                                    <label>Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="gender" id="genderProf">
                                        <option value="" selected>Pilih</option>
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Kelas</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="class_id" id="class_id">
                                        <option value="" selected>Pilih</option>
                                        @foreach($class as $classes)
                                            <option value="{{ $classes->id }}">{{ $classes->class }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="user_id" id="id_pfl" value="">
                        <input type="hidden" name="button_act" id="button_act" value="updated">
                        <input type="submit" name="submit" id="act" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="importModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('guru.siswa.importExcel') }}" id="importForm" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Excel</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>File Excel<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="file" id="import_file" name="import_file" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-sm">
                            Import
                        </button>
                        <a href="javascript:void(0);" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/users/siswa/siswa.js') }}"></script>
    <script src="{{ asset('js/users/siswa/siswa-table.js') }}"></script>
    <script src="{{ asset('js/users/siswa/siswa-ajax.js') }}"></script>
@stop
