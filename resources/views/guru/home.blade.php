@extends('guru.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card img {
            width: 65%;
            margin: 0 auto;
            display: block;
        }
        .brand-card {
            padding: 35px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .profil-identity label {
            text-align: center; 
            display: block; 
            font-size: 18px; 
            color: #fff; 
            font-weight: 600;
        }
    </style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Home</h1>

      <div class="row" id="main-content">
          <div class="col-sm-6">
              <div class="card shadow h-100">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Welcome, @foreach($guru as $profil){{ $profil->name }}@endforeach</h6>
                </div>
                <div class="card-body">
                    <div class="brand-card">
                        <img src="{{ asset('img/ayocbt_dark.png') }}">
                    </div>
                </div>
              </div>
          </div>
          <div class="col-sm-6">
                <div class="card shadow h-100">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Profil Saya</h6>
                    </div>
                    <div class="card-body">
                        <span id="form_output"></span>
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="card" style="background-color: #4E73DF">
                                    <div class="card-body">
                                        <img src="{{ asset('img/user.png') }}" class="img-profile rounded-circle" style="width: 50% !important;">
                                        <br>
                                        @foreach($guru as $profil)
                                            <div class="profil-identity">
                                                <label>{{ $profil->name }}</label>
                                                <label style="font-size: 12px;">{{ $profil->id }}</label>
                                                <span style="font-size: 12px; text-transform: capitalize; color: #fff; display: block; text-align: center;">Role : {{ $profil->role }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="home-profile">
                                    <div class="card">
                                        <div class="card-body">
                                            @foreach($guru as $profil)
                                                <div class="row" style="margin-bottom: 15px;">
                                                    <div class="col-sm-3">
                                                        <label class="control-label bold">Nama</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" id="name_show" value="{{ $profil->name }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-bottom: 15px;">
                                                    <div class="col-sm-3">
                                                        <label class="control-label bold">Email</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" id="email_show" value="{{ $profil->email }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-bottom: 15px;">
                                                    <div class="col-sm-3">
                                                        <label class="control-label bold">Username</label>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" id="username_show" value="{{ $profil->username }}" readonly>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="left">
                            <a href="#" class="btn btn-outline-secondary btn-sm edit-akses" data-toggle="modal" data-target="#akses_modal_home" id="{{ Auth::user()->id }}">
                                <i class="fas fa-unlock-alt"></i>
                                &nbsp;Edit Akses User
                            </a>
                            <!-- <a href="#" class="btn btn-primary btn-icon-split btn-sm">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Edit Profil</span>
                            </a> -->
                        </div>
                    </div>
                </div>
          </div>
      </div>

    </div>
    <!-- /.container-fluid -->

    <div id="akses_modal_home" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="akses_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Akses User</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div id="akses_bio">
                            <div class="form-group">
                                <label>Email<span style="color:red;">*</span></label>
                                <input type="email" id="emailAkses" name="email" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label>Username<span style="color:red;">*</span></label>
                                <input type="text" id="usernameAkses" name="username" class="form-control">
                            </div>
                        </div>
                        <div id="akses_pswd">
                            <div class="form-group">
                                <label>Password<span style="color: red">*</span></label>
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password<span style="color: red">*</span></label>
                                <input id="password-confirm" type="password" class="form-control pswd-conf" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group" style="margin-left: 5px;">
                            <input type="checkbox" class="check-pass" name="check" value="">
                            <label for="check_pswd"><b>Ganti Password Anda?</b></label>
                        </div>
                        <input type="hidden" name="role" class="form-control" value="admin">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <script type="text/javascript" src="{{ asset('js/home/guru/home.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/home/guru/home-ajax.js') }}"></script>
@stop
