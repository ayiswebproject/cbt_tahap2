@extends('guru.layouts.header-footer')

@section('css-app')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Hasil Ujian</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Hasil Ujian</h6>
          <a href="{{ url('/haiGuru/ujian') }}">
              <button class="btn btn-secondary btn-sm">
                <i class="fas fa-arrow-left"></i>
                Kembali
              </button>
          </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="hasil_test" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Kode Ujian</th>
                        <th>Nama Ujian</th>
                        <th>Pelajaran</th>
                        <th>Nilai</th>
                        <th>Dikerjakan Oleh</th>
                    </tr>
                </thead>
                <tbody>
                      @foreach($hasilUjian as $nilai)
                        @if($nilai->results == null)
                          
                        @else
                          <tr>
                              <td>{{ $nilai->code_test }}</td>
                              <td>{{ $nilai->name_test }}</td>
                              <td>{{ $nilai->subjects }}</td>
                              <td>{{ number_format($nilai->results, 2, '.', ',') }}</td>
                              <td>{{ $nilai->name }}</td>
                          </tr>
                        @endif
                      @endforeach
                </tbody>
            </table>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#hasil_test').DataTable();
        });
    </script>
@stop
