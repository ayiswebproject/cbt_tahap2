@extends('guru.layouts.header-footer')

@section('css-app')
<style type="text/css">
    ul.sidebar-dark {
        display: none !important;
    }
    .container-fluid.container-soal {
        margin-top: 25px;
        margin-bottom: 50px;
    }
    .btn-group .btn {
        border-top-left-radius: 0.35rem !important;
        border-bottom-left-radius: 0.35rem !important;
        border-top-right-radius: 0.35rem !important;
        border-bottom-right-radius: 0.35rem !important;
        margin-top: 15px;
    }
    .table td {
        font-size: 14px !important;
        vertical-align: baseline;
    }
    .form-updateDetail .form-control {
        font-size: 12px;
    }
    .name-app.text {
        display: none;
    }
    /*.btn.btn-success {
        display: none;
    }*/
</style>
@stop

@section('no-back-page')
<script type="text/javascript" >
   function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};

    document.onkeydown = function() {    
        switch (event.keyCode) { 
            case 116 : //F5 button
                event.returnValue = false;
                event.keyCode = 0;
                return false; 
            case 82 : //R button
                if (event.ctrlKey) { 
                    event.returnValue = false; 
                    event.keyCode = 0;  
                    return false; 
                } 
        }
    }
</script>
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid container-soal">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 mb-4">Tambah Soal Ujian</h1>

        <div class="row">
            <div class="col-sm-4">
                <div class="card" style="box-shadow: 1px 1px 10px rgba(58, 59, 69, 0.15); margin-top: 5px;">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">Detail Ujian</h6>
                    </div>
                    <div class="card-body">
                        @foreach($check as $details)
                            <form action="@if($details->jml_soal == 0){{ route('guru.ujian.updateDetail', $details->id) }}@else{{ route('guru.ujian.updateDetailOnAdd', $details->id) }}@endif" class="form-updateDetail" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <table class="table table-borderless">
                                    <tr>
                                        <td style="font-weight: bold;">Kode Ujian</td>
                                        <td>
                                            <input type="text" name="code_test" class="form-control" value="{{ $details->code_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Nama Ujian</td>
                                        <td>
                                            <input type="text" name="name_test" class="form-control" value="{{ $details->name_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Durasi</td>
                                        <td>
                                            <input type="text" name="duration" class="form-control" value="{{ $details->duration }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Type Ujian</td>
                                        <td>
                                            <input type="text" name="type_test" class="form-control" value="{{ $details->type_test }}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Nama Mapel</td>
                                        <td>
                                            <input type="text" name="subjects_id" class="form-control" value="{{ $details->subjects }}" disabled>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;">Jumlah Soal</td>
                                        <td>
                                            @if($details->jml_soal == null)
                                                <input type="text" name="jml_soal" class="form-control" id="jml_soal" value="" readonly>
                                            @else
                                                <input type="text" name="jml_soal" class="form-control" id="soal_jml" value="" readonly>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-group">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="fa fa-check"></i>
                                        Selesai
                                    </button>
                                </div>
                            </form>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <form id="add_soal" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="card" style="box-shadow: 1px 1px 10px rgba(58, 59, 69, 0.15); margin-top: 5px;">
                        <div class="card-header">
                            <h6 class="m-0 font-weight-bold text-primary">Form Soal</h6>
                            @foreach($check as $testid)
                            <input type="hidden" name="test_id" id="test_id" value="{{ $testid->id }}">
                            @endforeach
                        </div>
                        <div class="card-body">

                            <span id="form_output"></span>
                            <span id="error_output"></span>

                            <div class="form-group">
                                <label id="soal">Soal Ujian<span style="color: red">*</span></label>
                                <textarea for="soal" class="form-control" name="questions" rows="5" value="" id="questions" required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="file" name="quest_img" id="quest_img">
                            </div>
                            @foreach($check as $value)
                            @if($value->type_test == 'Pilihan Ganda')
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label id="a_field">Pilihan A<span style="color: red">*</span></label>
                                            <textarea for="a_field" class="form-control" name="a" value="" id="a" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="b_field">Pilihan B<span style="color: red">*</span></label>
                                            <textarea for="b_field" class="form-control" name="b" value="" id="b" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="c_field">Pilihan C<span style="color: red">*</span></label>
                                            <textarea for="c_field" class="form-control" name="c" value="" id="c" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label id="d_field">Pilihan D<span style="color: red">*</span></label>
                                            <textarea for="d_field" class="form-control" name="d" value="" id="d" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label id="e_field">Pilihan E<span style="color: red">*</span></label>
                                            <textarea for="e_field" class="form-control" name="e" value="" id="e" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                       <div class="form-group">
                                            <label id="answer_field_key">Kunci Jawaban<span style="color: red">*</span></label>
                                            <!-- <input for="answer_field_key" type="text" class="form-control" name="answer_key" id="answer_key" required> -->
                                            <table width="75%">
                                                <tr>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_a" value="a" required>
                                                        <label for="key_a" style="font-weight: 400;">A</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_b" value="b" required>
                                                        <label for="key_b" style="font-weight: 400;">B</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_c" value="c" required>
                                                        <label for="key_c" style="font-weight: 400;">C</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_d" value="d" required>
                                                        <label for="key_d" style="font-weight: 400;">D</label>
                                                    </td>
                                                    <td>
                                                        <input type="radio" name="answer_key" id="key_e" value="e" required>
                                                        <label for="key_e" style="font-weight: 400;">E</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div> 
                                    </div>
                                </div>
                            @elseif($value->type_test == 'Essay/Uraian')
                                <div class="form-group">
                                    <label id="answer_essay_key">Kunci Jawaban<span style="color: red">*</span></label>
                                    <textarea for="answer_essay_key" class="form-control" name="answer_essay" value="" id="answer_essay" required></textarea>
                                </div>
                            @endif
                            @endforeach
                            <div class="btn-group">
                                <input type="hidden" name="button_action" id="button_action" value="insert">
                                <input type="submit" name="submit" id="action" value="Tambah Soal" class="btn btn-primary btn-sm">
                                <!-- <a href="{{ route('ujian') }}" class="btn btn-success" style="margin-left: 10px;">
                                    <i class="fa fa-check"></i>
                                    Selesai
                                </a> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function() {

        var jml_soal = 0;
        var soal_jml = '{{ $details->jml_soal }}';
        // $('.btn.btn-success').hide();
        $('#jml_soal').val(0);
        $('#soal_jml').val('{{ $details->jml_soal }}');
        $('#add_soal').on('submit', function(event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url:"{{ route('guru.ujian.storeSoal') }}",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                dataType: 'json',
                success:function(data)
                {
                    if (data.error.length > 0) {
                        var error_html = '';
                        for (var count = 0; count < data.error.length; count++) {
                            error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                        }
                        $('#error_output').html(error_html);
                        $('#error_output').show();
                    } else {
                        jml_soal++;
                        soal_jml++;
                        $('#form_output').html(data.success);
                        $('#add_soal')[0].reset();
                        $('#jml_soal').val(jml_soal);
                        $('#soal_jml').val(soal_jml);
                        $('#button_action').val('insert');
                        $('#form_output').show();
                        $('#error_output').hide();
                        // $('.btn.btn-success').show();
                    }
                }
            })
        });
    });
</script>
@stop
