@extends('guru.layouts.header-footer')

@section('css-app')
<style type="text/css">
    table {
        margin-bottom: 0 !important;
    }
    .bg-nav-soal {
        background-color: #1CC88A;
        border-radius: 50%;
        padding: 10px 8px 10px 8px;
        box-shadow: 1px 1px 5px #b4b4b4;
        display: block;
        margin: 0 auto;
        margin-bottom: 30px;
        color: #fff;
    }
    .bg-nav-soal span {
        text-align: center;
        display: block;
    }
    .bg-nav-soal:hover {
        background-color: #1CC88A;
        color: #fff;
    }
    .active .bg-nav-soal {
        background-color: #1CC88A !important;
        color: #fff !important; 
    }
    .nav-keterangan {
        margin-top: 20px;
    }
    .nav-keterangan .badge {
        margin-bottom: 7px;
        margin-right: 7px;
    }
    .nav-keterangan .badge-light {
        background-color: #f0f0f0;
    }
    .nav-keterangan .badge-success {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 11px 10px 11px;
        border-radius: 50%;
    }
    .nav-keterangan .badge-light {
        box-shadow: 1px 1px 2px #b4b4b4;
        padding: 10px 13px 10px 13px;
        border-radius: 50%;
    }
    .card-header a.btn,
    .card-header button {
        float: right;
        margin-top: -20px;
        color: #fff;
    }
    .numbering {
        vertical-align: text-top;
    }
    .questions .form-check {
        padding: 10px 25px;
    }
    .carousel-indicators {
        position: relative;
        margin-right: 7%;
        margin-left: 7%;
    }
    .carousel-indicators span {
        width: 30px;
    }
    form label {
        font-weight: bold;
    }
    ul.sidebar-dark {
        display: none !important;
    }
    .btn-info {
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
        cursor: pointer;
        color: #fff !important;
    }
    .carousel-inner td {
        font-size: 14px;
    }
    .answer label {
        font-weight: bold;
        font-size: 14px;
    }
    .answer span {
        font-weight: 400;
        font-size: 14px;
    }
    span#key {
        text-transform: uppercase;
    }
    .modal-dialog {
        max-width: 1024px !important;
    }
    .name-app.text {
        display: none;
    }
</style>
@stop

@section('sidebar-app')
    
@stop

@section('top-bar-img')
<div class="top name-app">
    <img src="{{ asset('img/ayocbt_dark.png') }}" style="width: 35%;">
</div>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Soal Ujian</h1>
                <br>
                @if(Session::has('alert-success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                    </div>
                @endif
                @if(Session::has('alert-info'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                    </div>
                @endif
                @if(Session::has('alert-danger'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                    </div>
                @endif

                <!-- DataTales Example -->
                <div id="soalNavigation" class="carousel slide row" data-ride="carousel" data-interval="false">
                    <div class="col-sm-4">
                        <div class="card shadow mb-4" id="data_soal">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Navigasi Soal</h6>
                                @foreach($check as $add)
                                    <a href="{{ route('guru.ujian.addSoal', $add->id) }}">
                                        <button class="btn btn-primary btn-sm" id="add_soal" title="Tambah Soal">
                                            <i class="fas fa-plus"></i>
                                            Tambah Soal
                                        </button>
                                    </a>
                                @endforeach
                            </div>
                            <div class="card-body">
                                <div style="overflow-y: auto; height: 325px;">
                                    <div class="container" style="margin: 10px 0px 10px 0px;">
                                        <div class="row carousel-indicators">
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach($getSoal as $gets)
                                                <div class="col-sm-3 {{ $loop->first ? 'active' : '' }}">
                                                    <a data-target="#soalNavigation" data-slide-to="{{ $loop->index }}">
                                                        <button class="btn bg-nav-soal">
                                                            <span>{{ $i++ }}</span>
                                                        </button>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="nav-keterangan">
                                    <div class="ket-done">
                                        <span class="badge badge-success">
                                            <i class="fas fa-check"></i>
                                        </span> Soal Sudah Dijawab
                                    </div>
                                    <div class="ket-false">
                                        <span class="badge badge-light">
                                            <i class="fas fa-times"></i>
                                        </span> Soal Belum Dijawab
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Soal Ujian</h6>
                                <a href="{{ route('guru.ujian') }}">
                                    <button class="btn btn-secondary btn-sm">
                                        <i class="fas fa-arrow-left"></i>
                                        Kembali
                                    </button>
                                </a>
                            </div>
                            <div class="card-body">
                                <div class="container" style="margin: 10px 0px 10px 0px;">
                                <span id="form_output"></span>
                                  <div class="carousel-inner">
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($getSoal as $gets)
                                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                            <table width="100%" id="soal-list">
                                                <tr>
                                                    
                                                    <td class="numbering" width="7%">
                                                        <h5>{{ $no++ }}.</h5>
                                                    </td>
                                                    <td>
                                                        <span id="questions_view">{{ $gets->questions }}</span>
                                                        <br><br>
                                                        @if( $gets->quest_img != null )
                                                            <img src="{{ asset('uploads/soal/'.$gets->quest_img) }}" style="width: 20%"><br><br>
                                                        @else

                                                        @endif
                                                    </td>
                                                </tr>
                                                @foreach($check as $datas)
                                                @if( $datas->type_test == 'Pilihan Ganda')
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="answer">
                                                            <table class="table table-borderless" width="100%">
                                                                <tr>
                                                                    <td width="5%" style="vertical-align: text-top;">
                                                                        <label>A.</label>
                                                                    </td>
                                                                    <td>
                                                                        <span id="a_view">{{ $gets->a }}</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="answer">
                                                            <table class="table table-borderless" width="100%">
                                                                <tr>
                                                                    <td width="5%" style="vertical-align: text-top;">
                                                                        <label>B.</label>
                                                                    </td>
                                                                    <td>
                                                                        <span id="a_view">{{ $gets->b }}</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="answer">
                                                            <table class="table table-borderless" width="100%">
                                                                <tr>
                                                                    <td width="5%" style="vertical-align: text-top;">
                                                                        <label>C.</label>
                                                                    </td>
                                                                    <td>
                                                                        <span id="a_view">{{ $gets->c }}</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="answer">
                                                            <table class="table table-borderless" width="100%">
                                                                <tr>
                                                                    <td width="5%" style="vertical-align: text-top;">
                                                                        <label>D.</label>
                                                                    </td>
                                                                    <td>
                                                                        <span id="a_view">{{ $gets->d }}</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <div class="answer">
                                                            <table class="table table-borderless" width="100%">
                                                                <tr>
                                                                    <td width="5%" style="vertical-align: text-top;">
                                                                        <label>E.</label>
                                                                    </td>
                                                                    <td>
                                                                        <span id="a_view">{{ $gets->e }}</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <br>
                                                        <br>
                                                        <div class="answer">
                                                            <label>Kunci Jawaban :</label>&emsp;<span id="key">{{ $gets->answer_key }}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <label>Kunci Jawaban :</label>&emsp;<span id="key_essay">{{ $gets->answer_essay }}</span>
                                                    </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                            </table>
                                            <div class="btn-group">
                                                <a class="btn btn-info btn-sm edit-soal" href="{{ route('guru.ujian.editDetailSoal', $gets->id) }}">
                                                    <i class="fa fa-edit"></i>
                                                    Edit Soal
                                                </a>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="dropdown-divider"></div>
                                        </div>
                                    @endforeach
                                  </div>

                                    <br>
                                    <a href="#soalNavigation" data-slide="prev" class="btn btn-secondary" style="float: left;">
                                        <i class="fas fa-chevron-left"></i>
                                        Sebelumnya
                                    </a>
                                    <a href="#soalNavigation" data-slide="next" class="btn btn-primary" style="float: right;">
                                        Selanjutnya
                                        <i class="fas fa-chevron-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')
<script type="text/javascript">
    $(document).ready(function () {

        $('#selesai').click(function() {
            var count = 0;
            $('textarea').each(function(){
                if ($(this).val() == "") {
                    alert('gagal');
                    return false;
                }
                
            });
        });

    });
</script>
@stop
