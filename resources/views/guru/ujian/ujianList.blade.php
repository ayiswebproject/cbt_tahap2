@extends('guru.layouts.header-footer')

@section('css-app')
<link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<style type="text/css">
    td.dur:after {
        content: " menit";
    }
    td.jum:after {
        content: " soal";
    }
    .td,
    .dur,
    .jum {
        width: 10% !important;
    }
    .action {
        width: 15% !important;
    }
    .modal-dialog {
        max-width: 550px;
    }
</style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid" id="data_kelas">

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800 mb-4">Manajemen Ujian</h1>

      <!-- DataTales Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Data Ujian</h6>
          <a href="{{ url('/haiGuru/ujian/addForm') }}">
              <button class="btn btn-primary btn-sm">
                <i class="fas fa-plus"></i>
                Tambah
              </button>
          </a>
        </div>
        <div class="card-body">
            @if(Session::has('alert-success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-success') }}
                </div>
            @endif
            @if(Session::has('alert-info'))
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-info') }}
                </div>
            @endif
            @if(Session::has('alert-danger'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ \Illuminate\Support\Facades\Session::get('alert-danger') }}
                </div>
            @endif
            <span id="form_output"></span>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('guru.ujian') }}"><b>Pilihan Ganda</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('guru.ujianEssay') }}"><b>Essay/Uraian</b></a>
              </li>
            </ul>
            <br>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="test_table" style="width: 100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Kode Ujian</th>
                                <th>Nama Ujian</th>
                                <th>Durasi (menit)</th>
                                <th>Jumlah Soal</th>
                                <th>Tipe Ujian</th>
                                <th>Pelajaran</th>
                                <th>Dibuat Oleh</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <div id="detail_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="detail_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Detail Data Ujian</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Kode Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="code_test" name="id" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nama Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name_test" name="name_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Durasi (menit)<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="duration" name="duration" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Type Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="type_test" name="type_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="subjects" name="subjects" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dibuat Oleh<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="username" name="username" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="edit_form">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit Data Ujian</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <span id="error_output"></span>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Kode Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="code_test_edit" name="code_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Nama Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="name_test_edit" name="name_test" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Durasi (menit)<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="duration_edit" name="duration" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Type Ujian<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="type_test_edit" name="type_test" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Pelajaran<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" name="subjects_id" id="subjects_id">
                                        <option value="" selected>Pilih</option>
                                        @foreach($mapel as $mapels)
                                            <option value="{{ $mapels->id }}">{{ $mapels->subjects }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Dibuat Oleh<span style="color:red;">*</span></label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" id="username_get" name="username" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id" id="id_get" value="">
                        <input type="hidden" name="button_action" id="button_action" value="update">
                        <input type="submit" name="submit" id="action" value="Edit" class="btn btn-primary btn-sm">
                        <button type="button" class="btn btn-secondary btn-sm close-btn" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('js-app')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/ujian/pilgad/pilgad-table.js') }}"></script>
    <script src="{{ asset('js/ujian/pilgad/pilgad-ajax.js') }}"></script>
    <script src="{{ asset('js/ujian/ujian.js') }}"></script>
@stop
