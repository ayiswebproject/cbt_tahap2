<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>Dashboard AYO CBT | Aplikasi CBT Online</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

  <link rel="shortcut icon" href="{{ asset('img/icon.ico') }}">

  @yield('css-app')

  @yield('no-back-page')

</head>

<style type="text/css">
  body {
    background-color: #F8F9FC;
  }
  .logo-sidebar {
    width: 55%;
  }
  .sidebar-brand {
    margin: 7px 0;
    padding: 0 !important;
  }
</style>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <div class="sidebar-brand d-flex align-items-center justify-content-center">
        <img src="{{ asset('img/ayocbt_white.png') }}" class="logo-sidebar">
      </div>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <li class="nav-item
        {{ Route::currentRouteName() == 'homeGuru' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('homeGuru') }}">
          <i class="fas fa-fw fa-home"></i>
          <span>Home</span></a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading">
        Menu
      </div>

      <li class="nav-item
        {{ Route::currentRouteName() == 'guru.kelas' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('guru.kelas') }}">
          <i class="fas fa-fw fa-chalkboard"></i>
          <span>Manajemen Kelas</span></a>
      </li>

      <li class="nav-item
        {{ Route::currentRouteName() == 'guru.siswa' ? 'active' : '' }}
        {{ Route::currentRouteName() == 'guru.siswa.addForm' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('guru.siswa') }}">
          <i class="fas fa-fw fa-user-tie"></i>
          <span>Manajemen Siswa</span></a>
      </li>

      <li class="nav-item
        {{ Route::currentRouteName() == 'guru.ujian' ? 'active' : '' }}
        {{ Route::currentRouteName() == 'guru.ujianEssay' ? 'active' : '' }}
        {{ Route::currentRouteName() == 'guru.ujian.lihatHasil' ? 'active' : '' }}
        {{ Route::currentRouteName() == 'guru.ujian.addForm' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('guru.ujian') }}">
          <i class="fas fa-fw fa-book-reader"></i>
          <span>Manajemen Ujian</span></a>
      </li>

      <li class="nav-item
      {{ Route::currentRouteName() == 'guru.laporan' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('guru.laporan') }}">
          <i class="fas fa-fw fa-newspaper"></i>
          <span>Laporan</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          @yield('top-bar-img')

          <div class="top name-app text">
            Dashboard Guru
          </div>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small" id="name-dropdown">{{ Auth::user()->username }}</span>
                <img class="img-profile rounded-circle" src="{{ asset('img/user.png') }}">
              </a>
              <!-- Dropdown - User Information -->
              @if(Auth::user()->role == 'admin')
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
              @else
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
              @endif
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        @yield('main-app')

      </div>
      <!-- End of Main Content -->

      <div class="main-divider"></div>

      <!-- Footer -->
      <footer class="">
        <div class="copyright my-auto">
          <span>Copyright &copy; <b>AYO CBT</b> | Aplikasi CBT Online</span>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('js/jquery-3.4.1.js') }}"></script>
  <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    });
  </script>

  @yield('js-app')

</body>

</html>