<!DOCTYPE html>
<html lang="en">

  <style type="text/css">
    html, body {
      height: 100%;
    }
    .text-center {
      text-align: center;
    }
    .text-left {
      text-align: left;
    }
    .text-right {
      text-align: right;
    }
    table th {
      font-size: 14px;
      padding: 10px;
    }
    table td {
      font-size: 12px;
      padding: 7px;
    }
    table {
      width: 100%;
      border: 1px solid #313131;
      border-collapse: collapse;
    }
    table > thead > tr > th,
    table > tbody > tr > td {
      border: 1px solid #313131;
    }
    table th {
      text-align: center;
    }
  </style>

  <body style="font-family: sans-serif;">

    <header>
      <h2 class="text-center">Aplikasi CBT - Online</h2>
      <h3 class="text-center">Laporan Ujian Kelas 
        <?php $count = 0; ?>
        @foreach($report as $detail)
          <?php if($count == 1) break; ?>
          {{ $detail->class }}
          <?php $count++; ?>
        @endforeach
      </h3>
    </header>

    <hr>
    <br>
    <br>

    <div class="main-section">
      <table>
        <thead>
          <tr>
            <th>Nama</th>
            <th>No. Induk</th>
            <th>Kelas</th>
            <th>Kode Ujian</th>
            <th>Nama Ujian</th>
            <th>Nilai</th>
          </tr>
        </thead>
        <tbody>
          @foreach($report as $detail)
            <tr>
              <td class="text-center">{{ $detail->name }}</td>
              <td class="text-center">{{ $detail->id }}</td>
              <td class="text-center">{{ $detail->class }}</td>
              <td class="text-center">{{ $detail->code_test }}</td>
              <td class="text-center">{{ $detail->name_test }}</td>
              <td class="text-right">{{ number_format($detail->results, 2, '.', ',') }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </body>

</html>