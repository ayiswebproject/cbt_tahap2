@extends( Auth::user()->role == 'admin' ? 'admin.layouts.header-footer' : 'guru.layouts.header-footer')

@section('css-app')
    <style type="text/css">
        #main-content h1 {
            text-align: center;
            font-style: italic;
            font-size: 72pt;
        }
        @media (max-width: 600px) {
            #main-content h1 {
                font-size: 42px;
            }
        }
        #main-content h5 {
            text-align: center;
            font-size: 18pt;
        }
        @media (max-width: 600px) {
            #main-content h5 {
                font-size: 16px;
            }
        }
        #main-content .left {
            float: right;
        }
        .col-sm-6 {
            margin-bottom: 25px;
        }
        .brand-card img {
            width: 65%;
            margin: 0 auto;
            display: block;
        }
        .brand-card {
            padding: 35px 35px;
        }
        @media (max-width: 600px) {
            .brand-card {
                padding: 0px 0px;
            }
        }
        .profil-identity label {
            text-align: center; 
            display: block; 
            font-size: 18px; 
            color: #fff; 
            font-weight: 600;
        }
    </style>
@stop

@section('main-app')
    <!-- Begin Page Content -->
    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Laporan</h1>

        <div class="row">
          <div class="col-sm-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Laporan Hasil Ujian</h6>
                </div>
                <div class="card-body">
                    <form action="@if(Auth::user()->role == 'admin'){{ route('downloadLaporan') }}@elseif(Auth::user()->role == 'guru'){{ route('guru.downloadLaporan') }}@endif" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group mb-4">
                            <select class="form-control" name="select_class" required>
                                <option value="" selected>Pilih Kelas</option>
                                @foreach($class as $classes)
                                    <option value="{{ $classes->id }}">{{ $classes->class }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-4">
                            <select class="form-control" name="select_code_test" required>
                                <option value="" selected>Pilih Kode Ujian</option>
                                @foreach($ujian as $exam)
                                    <option value="{{ $exam->id }}">{{ $exam->name_test }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button class="btn btn-primary" style="float: right;">
                            <i class="fas fa-check"></i>&nbsp; Buat Laporan
                        </button>
                    </form>
                </div>
            </div>
          </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@stop

@section('js-app')

@stop
