$(document).ready(function() {

	$('.close, .close-btn').click(function() {
        $('#error_output').hide();
    });

    $('#add').click(function() {
        $('#add_class_form')[0].reset();
        $('#form_output').html('');
        $('#button_action').val('insert');
        $('#action').val('Tambah');
        $('.modal-title').html('Tambah Data Kelas');
        $('#class_id').prop('readonly', false);
        $('#class').prop('readonly', false);
        $('#form_output').hide();
        $('#action').show();
    });
    
});