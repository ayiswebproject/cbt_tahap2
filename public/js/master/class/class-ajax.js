$(document).ready(function() {

	$('#add_class_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"kelas/store",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_output').html(error_html);
                    $('#error_output').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#add_class_form')[0].reset();
                    $('#button_action').val('insert');
                    $('#class_table').DataTable().ajax.reload();
                    $('#action').show();
                    $('#class_id').prop('readonly', false);
                    $('#class').prop('readonly', false);
                    $('#form_output').show();
                    $('#error_output').hide();
                    $('#class_modal').modal('hide');
                }
            }
        })
    });

    $(document).on('click', '.detail', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"kelas/fetchdata",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#class').val(data.class);
                $('#class_id').val(id);
                $('.modal-title').text('Detail Data Kelas');
                $('#action').hide();
                $('#class_id').prop('readonly', true);
                $('#class').prop('readonly', true);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.edit', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"kelas/fetchedit",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#class').val(data.class);
                $('#class_id').val(id);
                $('#action').val('Edit');
                $('.modal-title').text('Edit Data Kelas');
                $('#button_action').val('update');
                $('#action').show();
                $('#class_id').prop('readonly', true);
                $('#class').prop('readonly', false);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.delete', function() {
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin akan menghapus Data ini?"))
        {
            $.ajax({
                url:"kelas/delete",
                method:"GET",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#class_table').DataTable().ajax.reload();
                    $('#form_output, #error_output').hide();
                }
            }) 
        }
        else {
            return false;
        }
    });

});