$(document).ready(function() {

	$('#class_table').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "asc" ]],
        "ajax": "kelas/getData",
        "columns": [
            { 
                "data": "id",
                "width": "30%"
            },
            { 
                "data": "class",
                "width": "30%"
            },
            { 
                "data": "action",
                "orderable": "false",
                "searchable": "false",
                "width": "30%"
            }
        ]
    });
    
});