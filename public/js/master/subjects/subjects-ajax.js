$(document).ready(function() {

	$('#add_mapel_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"mapel/store",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_output').html(error_html);
                    $('#error_output').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#add_mapel_form')[0].reset();
                    $('#button_action').val('insert');
                    $('#mapel_table').DataTable().ajax.reload();
                    $('#action').show();
                    $('#subjects_id').prop('readonly', false);
                    $('#subjects').prop('readonly', false);
                    $('#form_output').show();
                    $('#error_output').hide();
                    $('#mapel_modal').modal('hide');
                }
            }
        })
    });

    $(document).on('click', '.detail', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"mapel/fetchdata",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#subjects').val(data.subjects);
                $('#subjects_id').val(id);
                $('.modal-title').text('Detail Data Mapel');
                $('#action').hide();
                $('#subjects_id').prop('readonly', true);
                $('#subjects').prop('readonly', true);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.edit', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"mapel/fetchedit",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#subjects').val(data.subjects);
                $('#subjects_id').val(id);
                $('#action').val('Edit');
                $('.modal-title').text('Edit Data Mapel');
                $('#button_action').val('update');
                $('#action').show();
                $('#subjects_id').prop('readonly', true);
                $('#subjects').prop('readonly', false);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.delete', function() {
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin akan menghapus Data ini?"))
        {
            $.ajax({
                url:"mapel/delete",
                method:"GET",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#mapel_table').DataTable().ajax.reload();
                    $('#form_output, #error_output').hide();
                }
            }) 
        }
        else {
            return false;
        }
    });

});