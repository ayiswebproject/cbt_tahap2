$(document).ready(function() {

    $('#mapel_table').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "desc" ]],
        "ajax": "mapel/getData",
        "columns": [
            { 
                "data": "id",
                "width": "30%"
            },
            { 
                "data": "subjects",
                "width": "30%"
            },
            { 
                "data": "action",
                "orderable": "false",
                "searchable": "false",
                "width": "30%"
            }
        ]
    });

});