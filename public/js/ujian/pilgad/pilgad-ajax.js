$(document).ready(function() {

    $('#edit_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"ujian/updateUjian",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_output').html(error_html);
                    $('#error_output').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#edit_form')[0].reset();
                    $('#button_act').val('updated');
                    $('#test_table, #essay_table').DataTable().ajax.reload();
                    $('#edit_modal').modal('hide');
                    $('#form_output').show();
                    $('#error_output').hide();
                    $('#action').show();
                }
            }
        })
    });

    $(document).on('click', '.detail', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"ujian/fetchdataUjian",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#code_test').val(data.code_test);
                $('#name_test').val(data.name_test);
                $('#duration').val(data.duration);
                $('#type_test').val(data.type_test);
                $('#subjects').val(data.subjects);
                $('#username').val(data.username);
                $('#id').val(id);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.edit', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"ujian/fetchdataEditUjian",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#code_test_edit').val(data.code_test);
                $('#name_test_edit').val(data.name_test);
                $('#duration_edit').val(data.duration);
                $('#type_test_edit').val(data.type_test);
                $('#subjects_id').val(data.subjects_id);
                $('#username_get').val(data.username);
                $('#id_get').val(id);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.delete', function() {
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin akan menghapus Data ini?"))
        {
            $.ajax({
                url:"ujian/delete",
                method:"GET",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#test_table, #essay_table').DataTable().ajax.reload();
                    $('#form_output, #error_output, #error_profil').hide();
                }
            }) 
        }
        else {
            return false;
        }
    });

});