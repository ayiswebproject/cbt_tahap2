$(document).ready(function() {

	$('#essay_table').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [[ 1, "asc" ]],
        "ajax": "ujianEssay/getEssay",
        "columns": [
            { 
                "data": "code_test",
                "className":"td",
            },
            { 
                "data": "name_test",
                "className":"td",
            },
            { 
                "data": "duration",
                "className":"dur",
            },
            { 
                "data": "jml_soal",
                "className":"jum",
            },
            { 
                "data": "type_test",
                "className":"td",
            },
            { 
                "data": "subjects",
                "className":"td",
            },
            { 
                "data": "username",
                "className":"td",
            },
            { 
                "data": "action",
                "orderable": "false",
                "searchable": "false",
                "className":"action",
            }
        ]
    });

});