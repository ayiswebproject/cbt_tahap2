$(document).ready(function() {

	$('#akses_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"haiGuru/updateAkses",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_output').html(error_html);
                    $('#password').val('');
                    $('#password-confirm').val('');
                    $('#error_output').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#email_show').val(data.email);
                    $('#username_show').val(data.username);
                    $('#name-dropdown, .profil-identity label').html(data.username);
                    $('#akses_form')[0].reset();
                    $('#button_action').val('update');
                    $('#akses_modal_home').modal('hide');
                    $('#form_output').show();
                    $('#error_output').hide();
                    $('#action').show();
                    $('#password').prop('disabled', true);
                    $('.pswd-conf').prop('disabled', true);
                    $('#akses_pswd').hide();
                }
            }
        })
    });

    $(document).on('click', '.edit-akses', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"haiGuru/fetchdataAkses",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#emailAkses').val(data.email);
                $('#usernameAkses').val(data.username);
                $('#pswd_hide').val(data.pswd_hide);
                $('#action').val('Edit');
                $('.modal-title').text('Edit Akses User');
                $('#id').val(id);
                $('#form_output').hide();
            }
        })
    });

});