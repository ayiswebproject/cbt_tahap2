$(document).ready(function() {

	$('#guru_table').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [[ 0, "asc" ]],
        "ajax": "guru/getData",
        "columns": [
            { 
                "data": "id",
                "width": "10%"
            },
            { 
                "data": "name",
                "width": "10%"
            },
            { 
                "data": "email",
                "width": "10%"
            },
            { 
                "data": "username",
                "width": "10%"
            },
            { 
                "data": "gender",
                "width": "10%"
            },
            { 
                "data": "subjects",
                "width": "10%"
            },
            { 
                "data": "action",
                "orderable": "false",
                "searchable": "false",
                "width": "15%"
            }
        ]
    });

});