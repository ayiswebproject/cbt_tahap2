$(document).ready(function() {

	$('#akses_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"guru/updateAkses",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_output').html(error_html);
                    $('#password').val('');
                    $('#password-confirm').val('');
                    $('#error_output').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#akses_form')[0].reset();
                    $('#button_action').val('update');
                    $('#guru_table').DataTable().ajax.reload();
                    $('#akses_modal').modal('hide');
                    $('#form_output').show();
                    $('#error_output').hide();
                    $('#action').show();
                    $('#password').prop('disabled', true);
                    $('.pswd-conf').prop('disabled', true);
                    $('#akses_pswd').hide();
                }
            }
        })
    });

    $('#profil_form').on('submit', function(event) {
        event.preventDefault();

        var form_data = $(this).serialize();

        $.ajax({
            url:"guru/updateProfil",
            method:"POST",
            data:form_data,
            dataType:"json",
            success:function(data)
            {
                if (data.error.length > 0) {
                    var error_html = '';
                    for (var count = 0; count < data.error.length; count++) {
                        error_html += '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+data.error[count]+'</div>';
                    }
                    $('#error_profil').html(error_html);
                    $('#error_profil').show();
                } else {
                    $('#form_output').html(data.success);
                    $('#profil_form')[0].reset();
                    $('#button_act').val('updated');
                    $('#guru_table').DataTable().ajax.reload();
                    $('#profil_modal').modal('hide');
                    $('#form_output').show();
                    $('#error_profil').hide();
                    $('#act').show();
                }
            }
        })
    });

    $(document).on('click', '.detail', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"guru/fetchdata",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#username').val(data.username);
                $('#gender').val(data.gender);
                $('#subjects').val(data.subjects);
                $('.modal-title').text('Detail Profil Guru');
                $('#idd').val(id);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.edit-akses', function() {
        var id = $(this).attr('id');
        $.ajax({
            url:"guru/fetchdataAkses",
            method:"GET",
            data:{id:id},
            dataType:"json",
            success:function(data)
            {
                $('#emailAkses').val(data.email);
                $('#usernameAkses').val(data.username);
                $('#pswd_hide').val(data.pswd_hide);
                $('#action').val('Edit');
                $('.modal-title').text('Edit Akses User');
                $('#ids').val(id);
                $('#id').val(id);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.edit-profil', function() {
        var user_id = $(this).attr('id');
        $.ajax({
            url:"guru/fetchdataProfil",
            method:"GET",
            data:{id:user_id},
            dataType:"json",
            success:function(data)
            {
                $('#id_prof').val(data.id);
                $('#nameAkses').val(data.name);
                $('#genderProf').val(data.gender);
                $('#genderProf').val(data.gender);
                $('#subjects_id').val(data.subjects_id);
                $('#user_id').val(data.user_id);
                $('#act').val('Edit');
                $('.modal-title').text('Edit Profil Guru');
                $('#id_pfl').val(user_id);
                $('#form_output').hide();
            }
        })
    });

    $(document).on('click', '.delete', function() {
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin akan menghapus Data ini?"))
        {
            $.ajax({
                url:"guru/delete",
                method:"GET",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#guru_table').DataTable().ajax.reload();
                    $('#form_output, #error_output, #error_profil').hide();
                }
            }) 
        }
        else {
            return false;
        }
    });

});