$(document).ready(function() {

	$('.close, .close-btn').click(function() {
        $('#error_output, #error_profil').hide();
    });

    $('#password').prop('disabled', true);
    $('.pswd-conf').prop('disabled', true);
    $('#akses_pswd').hide();

    $(".check-pass").click(function() {
        if($('.check-pass').is(':checked')) { 
            $('#password').prop('disabled', false);
            $('.pswd-conf').prop('disabled', false);
            $('.check-pass').val('checked');
            $('#akses_pswd').show();
        } else {
            $('#password').prop('disabled', true);
            $('.pswd-conf').prop('disabled', true);
            $('.check-pass').val('');
            $('#akses_pswd').hide();
        }
    });
    
});